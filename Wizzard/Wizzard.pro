#-------------------------------------------------
#
# Project created by QtCreator 2014-01-06T10:03:25
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets webkitwidgets

TARGET = Wizzard
TEMPLATE = app


SOURCES += main.cpp\
        wizzard.cpp \
    wsmbconnect.cpp \
    wlearn.cpp \
    wweb.cpp \
    wtvreg.cpp \
    settings.cpp \
    lanscanner.cpp

HEADERS  += wizzard.h \
    wsmbconnect.h \
    wlearn.h \
    wweb.h \
    wtvreg.h \
    settings.h \
    lanscanner.h

FORMS    += wizzard.ui \
    wsmbconnect.ui \
    wlearn.ui \
    wweb.ui \
    wtvreg.ui \
    settings.ui

INCLUDEPATH += ../PicParse
LIBS     += -L../PicParse -lPicParse

INCLUDEPATH += /usr/include
LIBS += -L/usr/lib
LIBS += -lflycapture
LIBS += -ljpeg

INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core

include (obj/obj.pri)

RESOURCES += \
    Graphics.qrc
