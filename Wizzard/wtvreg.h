#ifndef WTVREG_H
#define WTVREG_H

#include <deque>

#include <QWidget>
#include <QTimer>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QSettings>

#include "smbconnection.h"
#include "rectselectscene.h"
#include "smarttvrecpack.h"
#include "picparse.h"

namespace Ui {
class WTvReg;
}

class WTvReg : public QWidget
{
    Q_OBJECT
signals:
     void finishedTvRect(QString url);
public:
    explicit WTvReg(QWidget *parent = 0);
    ~WTvReg();
    enum TvRegMode{Mark,Record};

    inline void setConnection(SmbConnection connection){this->connection = connection;}
    void setManagerAuth(QNetworkAccessManager * manager);

public slots:
    void startTvReg(QString postPath, QString cancelLink, TvRegMode mode, QRectF rect);
private:
    Ui::WTvReg *ui;
    enum Stage{Capturing,Marking,Recording,Editing,Sending};
    Stage status;
    TvRegMode mode;

    QTimer * timer;
    SmbConnection connection;
    RectSelectScene * scene;
    QGraphicsScene * recScene;

    bool capturing;
    bool recordingFrames;

    QString postPath;
    QSettings settings;
    QString cancelLink;

    std::deque<QByteArray> frames;

    QNetworkAccessManager * manager;
    QNetworkAccessManager * authManager;
    SmartTvRecPack * smarttvRectPack;

    void frameGrabbed(QByteArray jpg);

private slots:

    void grabFrame();
    void grabFrameFinished(QNetworkReply * reply);
    void showJpg(QByteArray jpg);
    void startCapture();
    void stopCapture();
    void smartFacePackFinished(SmartTvRecPack * smarttvRectPack);
    void smartFacePackFailed(SmartTvRecPack * smarttvRectPack);
    void showRecJpg(QByteArray jpg);

    void formatButtons();

    void btnStartStopClick();
    void btnSubmitClick();
    void btnCancelClick();
    void btnRecordClick();
    void btnClearLeftClick();
    void btnClearRightClick();
    void sliderValueChanged(int value);


};

#endif // WTVREG_H
