#include "wlearn.h"
#include "ui_wlearn.h"

WLearn::WLearn(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WLearn)
{
    localizerBusy = false;
    learnerBusy = false;
    smartFacesPack = 0;
    authManager = 0;
    timer = new QTimer(this);
    manager = new QNetworkAccessManager(this);
    scene = new QGraphicsScene(this);
    faceScene = new QGraphicsScene(this);

    qRegisterMetaType<cv::Mat *>("cv::Mat *");

    connect(timer,SIGNAL(timeout()),this,SLOT(grabFrame()));

    ui->setupUi(this);

    connect(ui->btnStart, SIGNAL(clicked()), this, SLOT(btnStartClick()));
    connect(ui->btnStop, SIGNAL(clicked()), this, SLOT(btnStopClick()));
    connect(ui->btnClear, SIGNAL(clicked()), this, SLOT(btnClearClick()));
    connect(ui->btnSend, SIGNAL(clicked()), this, SLOT(btnSendClick()));
    connect(ui->btnExport, SIGNAL(clicked()), this, SLOT(btnExportClick()));
    connect(ui->btnCancel, SIGNAL(clicked()), this, SLOT(btnCancelClick()));

    ui->preview->setScene(scene);
    ui->facePreview->setScene(faceScene);

    progressDefStyle = ui->modelProgress->property("defaultStyleSheet").toString();
    //    scene->setSceneRect(0,0,100,100);

    SmartSettings settings;
    settings.readFromFile("wizzardSettings.json");
    settings.dumpToFile("wizzardSettings.json");
    localizer.load(settings);
    connect(&localizer, SIGNAL(picParsed(cv::Mat *, cv::Mat *)), this, SLOT(picParsed(cv::Mat *, cv::Mat *)));
    connect(&learner, SIGNAL(faceAdded(double,int)), this, SLOT(faceAdded(double,int)));
    connect(this->manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(grabFrameFinished(QNetworkReply *)));
    connect(ui->modelProgress, SIGNAL(valueChanged(int)),this,SLOT(onProgress(int)));

    connect(&learner, SIGNAL(finished()), &learnerThread, SLOT(quit()));
    connect(&localizer, SIGNAL(finished()), &localizerThread, SLOT(quit()));

    localizer.moveToThread(&localizerThread);
    learner.moveToThread(&learnerThread);

    localizerThread.start();
    learnerThread.start();

    learning = false;
    capturing = false;
}

WLearn::~WLearn()
{
    learner.terminate();
    localizer.terminate();

    localizerThread.wait();
    learnerThread.wait();
    delete ui;
}

void WLearn::setManagerAuth(QNetworkAccessManager *manager)
{
    this->authManager = manager;
}

void WLearn::startCapture(QString clientId, QString cancelLink, QString postPath)
{
    this->cancelLink = cancelLink;
    ui->trainedFacesCount->setText(QString::number(learner.facesTrained()));
    this->clientId = clientId;
    this->postPath = postPath;
    timer->start(1000/12);
    ui->btnStop->setEnabled(false);
    ui->btnStart->setEnabled(true);
    ui->btnClear->setEnabled(true);
    ui->btnSend->setEnabled(false);
    capturing = true;
}

void WLearn::stopCapture()
{
    timer->stop();
    capturing = false;
}


void WLearn::grabFrame()
{
    if(manager == 0){
        qWarning() << "WLearn QNetworkAccessManager not set!";
        return;
    }
    QUrl url(QString("http://").append(connection.getIpPort().append("/grab/wc")));
    QNetworkRequest request(url);
    QNetworkReply * reply = manager->get(request);
    reply->setParent(this);
}


void WLearn::grabFrameFinished(QNetworkReply * reply)
{
    if(reply->error() != QNetworkReply::NoError){
        qDebug() << reply->errorString();
        return;
    }
    frameGrabbed(reply->readAll());
    reply->deleteLater();
}


void WLearn::frameGrabbed(QByteArray jpg)
{
    if(localizerBusy) return;
    cv::Mat * pic = SmartConv::JpgToMatP(&jpg);
    if(pic->empty()){
        delete pic;
        return;
    }
    localizerBusy = true;
    QMetaObject::invokeMethod(&localizer, "parsePic", Qt::QueuedConnection, Q_ARG( cv::Mat *, pic));
    //localizer.parsePic(pic);
}

void WLearn::picParsed(cv::Mat * pic, cv::Mat * face)
{
    localizerBusy = false;
    QByteArray resJpg = SmartConv::MatToJpg(*pic);
    if(face != 0) showFaceJpg(SmartConv::MatToJpg(*face));
    showJpg(resJpg);
    delete pic;

    if(face == 0) return;
    if(!learning){
        delete face;
        return;
    }
    if(learnerBusy){
        delete face;
        return;
    }
    learnerBusy = true;
    QMetaObject::invokeMethod(&learner, "addFace", Qt::QueuedConnection, Q_ARG( cv::Mat *, face));
    //learner.addFace(face);
}

void WLearn::faceAdded(double precision, int facesTrained)
{
    learnerBusy = false;
    if(learning) ui->modelProgress->setValue((int)precision);
    ui->trainedFacesCount->setText(QString::number(facesTrained));
}

void WLearn::showJpg(QByteArray jpg)
{
    scene->clear();
    if(jpg.isEmpty()) return;
    QImage image;
    image = QImage::fromData(jpg,"jpg");
    scene->addPixmap(QPixmap::fromImage(image));
}

void WLearn::btnStartClick()
{
    learning = true;
    ui->modelProgress->setValue(0);
    ui->modelProgress->setEnabled(true);
    ui->btnStop->setEnabled(true);
    ui->btnStart->setEnabled(false);
    ui->btnClear->setEnabled(false);
    ui->btnSend->setEnabled(false);
}

void WLearn::btnStopClick()
{
    learning = false;
    ui->modelProgress->setValue(0);
    ui->modelProgress->setEnabled(false);
    ui->btnStop->setEnabled(false);
    ui->btnStart->setEnabled(true);
    ui->btnClear->setEnabled(true);
    ui->btnSend->setEnabled(true);
}

void WLearn::btnClearClick()
{
    learning = false;
    ui->btnSend->setEnabled(false);
    learner.clear();
    ui->trainedFacesCount->setText(QString::number(learner.facesTrained()));
}

void WLearn::btnSendClick()
{
    if(authManager == 0) return;
    //
    std::vector<QByteArray *> faces = learner.getGoodFaces();
    if(faces.empty())
    {
        QMessageBox msgBox(this);
        msgBox.setText("Model empty! There are no faces to send to server.");
        msgBox.exec();
        return;
    }

    ui->btnStop->setEnabled(false);
    ui->btnStart->setEnabled(false);
    ui->btnClear->setEnabled(false);
    ui->btnSend->setEnabled(false);

    smartFacesPack = new SmartFacesPack(authManager,postPath,clientId,faces,this);
    connect(smartFacesPack,SIGNAL(finished(SmartFacesPack*)),this,SLOT(smartFacePackFinished(SmartFacesPack*)));
    connect(smartFacesPack,SIGNAL(failed(SmartFacesPack*)),this,SLOT(smartFacePackFailed(SmartFacesPack*)));
     smartFacesPack->send();
}

void WLearn::btnExportClick()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setNameFilter(tr("XML Documents (*.xml)"));
    dialog.setAcceptMode( QFileDialog::AcceptSave );

    QStringList fileNames;
    if (!dialog.exec())
    {
        return;
    }

    fileNames = dialog.selectedFiles();
    QString filename = fileNames[0];
    if(!filename.endsWith(".xml")) filename.append(".xml");
    qDebug() << "Exproting faces to" << filename;
    learner.saveToFile(filename);
}


void WLearn::smartFacePackFinished(SmartFacesPack * pack)
{
    pack->deleteLater();
    learner.clear();
    smartFacesPack = 0;
    timer->stop();
    capturing = false;
    emit finishedLearning(pack->getRedirectLink());
}

void WLearn::smartFacePackFailed(SmartFacesPack * pack)
{
    pack->deleteLater();
    smartFacesPack = 0;
    ui->btnStop->setEnabled(false);
    ui->btnStart->setEnabled(true);
    ui->btnClear->setEnabled(true);
    ui->btnSend->setEnabled(true);
    QMessageBox msgBox(this);
    msgBox.setText("Failed to send images to server.");
    msgBox.exec();
}

void WLearn::btnCancelClick()
{
    if(smartFacesPack != 0)
    {
        smartFacesPack->stop();
        smartFacesPack->deleteLater();
        ui->btnStop->setEnabled(false);
        ui->btnStart->setEnabled(true);
        ui->btnClear->setEnabled(true);
        ui->btnSend->setEnabled(true);
        smartFacesPack = 0;
    }else{
        learner.clear();
        qDebug() << "Canceling: " << cancelLink;
        timer->stop();
        capturing = false;
        emit finishedLearning(cancelLink);
    }
}


void WLearn::onProgress(int value)
{
    int green = (double)value/100.*200.;
    int red = 254. - std::max((value-30)/70,0)*254.;
    ui->modelProgress->setStyleSheet(progressDefStyle + " QProgressBar::chunk { background: rgb("+QString::number(red)+","+QString::number(green)+",0); }");
}

void WLearn::showFaceJpg(QByteArray jpg)
{
    if(jpg.isEmpty()) return;
    faceScene->clear();
    QImage image = QImage::fromData(jpg,"jpg");
    faceScene->setSceneRect(0,0,0,0);
    faceScene->addPixmap(QPixmap::fromImage(image));
    ui->facePreview->setTransform(QTransform());
    qreal scale;
    if(ui->facePreview->width()/ui->facePreview->height() >
            (double)image.width()/image.height()){
        scale = (double)ui->facePreview->height()/ image.height();
    }else{
        scale = (double)ui->facePreview->width()/ image.width();
    }
    ui->facePreview->scale(scale,scale);
}

