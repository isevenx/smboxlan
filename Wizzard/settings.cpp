#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
    ui->editFirstStepPath->setText(settings.value("Wizzard/startPath","/OtwGateway/wizard/device/registration/residence").toString());
    ui->editServerDomain->setText(settings.value("Wizzard/serverDomain","http://192.168.4.142:8080").toString());
}

Settings::~Settings()
{
    delete ui;
}


void Settings::accept()
{

    settings.setValue("Wizzard/startPath",ui->editFirstStepPath->text());
    settings.setValue("Wizzard/serverDomain",ui->editServerDomain->text());
    hide();
}

void Settings::reject()
{
    ui->editFirstStepPath->setText(settings.value("Wizzard/startPath","/OtwGateway/wizard/device/registration/residence").toString());
    ui->editServerDomain->setText(settings.value("Wizzard/serverDomain","http://192.168.4.142:8080").toString());
    hide();
}
