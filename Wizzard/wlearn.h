#ifndef WLEARN_H
#define WLEARN_H

#include <QWidget>
#include <QDebug>
#include <QTimer>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QGraphicsScene>
#include <QImage>
#include <QBuffer>
#include <QThread>
#include <QMessageBox>
#include <QFileDialog>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "smbconnection.h"
#include "settings/smartsettings.h"
#include "picparse.h"
#include "smartconv.h"
#include "facelearn.h"
#include "localizer.h"
#include "learner.h"
#include "smartfacespack.h"

namespace Ui {
class WLearn;
}

class WLearn : public QWidget
{
    Q_OBJECT
signals:
    void finishedLearning(QString redirectLink);
public:
    explicit WLearn(QWidget *parent = 0);
    ~WLearn();
    inline void setConnection(SmbConnection connection){this->connection = connection;}
    void setManagerAuth(QNetworkAccessManager * manager);

public slots:
    void startCapture(QString clientId, QString cancelLink, QString postPath);

private:
    Ui::WLearn *ui;
    QTimer * timer;
    SmbConnection connection;
    QGraphicsScene * scene;
    QGraphicsScene * faceScene;
    bool learning;
    bool capturing;
    QString progressDefStyle;

    Localizer localizer;
    QThread localizerThread;
    Learner learner;
    QThread learnerThread;
    bool localizerBusy;
    bool learnerBusy;

    QNetworkAccessManager * manager;
    QNetworkAccessManager * authManager;
    SmartFacesPack * smartFacesPack;

    QString clientId;
    QString postPath;
    QString cancelLink;

    void frameGrabbed(QByteArray jpg);

private slots:

    void grabFrame();
    void grabFrameFinished(QNetworkReply * reply);
    void picParsed(cv::Mat * pic, cv::Mat * face);
    void faceAdded(double precision, int facesTrained);
    void showJpg(QByteArray jpg);
    void showFaceJpg(QByteArray jpg);
    void stopCapture();
    void smartFacePackFinished(SmartFacesPack * pack);
    void smartFacePackFailed(SmartFacesPack * pack);

    void onProgress(int value);

    void btnStartClick();
    void btnStopClick();
    void btnClearClick();
    void btnSendClick();
    void btnExportClick();
    void btnCancelClick();
};

#endif // WLEARN_H
