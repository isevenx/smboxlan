#ifndef WIZZARD_H
#define WIZZARD_H

#include <QMainWindow>

#include "smbconnection.h"
#include "settings.h"

namespace Ui {
class Wizzard;
}

class Wizzard : public QMainWindow
{
    Q_OBJECT

public:
    explicit Wizzard(QWidget *parent = 0);
    ~Wizzard();
private:
    Ui::Wizzard *ui;

private slots:

    void smbConnected(SmbConnection connection);
    void testLearn(SmbConnection connection);
    void launchLearn(QString clientId, QString cancelLink, QString postPath);
    void launchTvReg(QString postPath, QString cancelLink, bool capturePic, bool canResize, QRectF rect);
    void finishedLearning(QString url);
    void finishedTvRect(QString url);
    void testRect(SmbConnection connection);

    void menuSettingsClick();
};

#endif // WIZZARD_H
