#ifndef WSMBCONNECT_H
#define WSMBCONNECT_H

#include <QWidget>
#include <QUdpSocket>
#include <QHostAddress>
#include <QList>
#include <QNetworkConfigurationManager>
#include <QListWidgetItem>
#include "lanscanner.h"
#include "obj/smbconnection.h"

namespace Ui {
class WSmbConnect;
}

class WSmbConnect : public QWidget
{
    Q_OBJECT
public:
    explicit WSmbConnect(QWidget *parent = 0);
    ~WSmbConnect();
     LanScanner lanscanner;
signals:
    void smbConnected(SmbConnection connection);
    void testLearn(SmbConnection connection);
    void testRect(SmbConnection connection);
public slots:
    void foundSmartBox(QString address);
    void btnPushPermission(int res);
private slots:
    void processUdpDatagrams();
    void pingSmb();
    void refreshIfaces();
    void ifaceChanged();

    void startClicked();
    void testLearnClicked();
    void testRectClicked();

    void smbSelected(QListWidgetItem * item );
    void ipTextChanged(QString text);

    void on_btnPingL_clicked();

private:

    Ui::WSmbConnect *ui;
    QUdpSocket * udpSocket;
    QHostAddress udpGroupAddress;
    quint64 port;
    QList<QString> smbIps;


    void setButtonStates();

};

#endif // WSMBCONNECT_H
