#include "wizzard.h"
#include "ui_wizzard.h"

Wizzard::Wizzard(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Wizzard)
{

    qRegisterMetaType<SmbConnection>("SmbConnection");
    ui->setupUi(this);

    ui->wLearn->setVisible(false);
    ui->wWeb->setVisible(false);
    ui->wTvReg->setVisible(false);

    connect(ui->wSmbConnect,SIGNAL(smbConnected(SmbConnection)),this,SLOT(smbConnected(SmbConnection)));
    connect(ui->wSmbConnect,SIGNAL(testLearn(SmbConnection)),this,SLOT(testLearn(SmbConnection)));
    connect(ui->wSmbConnect,SIGNAL(testRect(SmbConnection)),this,SLOT(testRect(SmbConnection)));
    connect(ui->wWeb,SIGNAL(launchLearn(QString,QString,QString)),this,SLOT(launchLearn(QString,QString,QString)));
    connect(ui->wWeb,SIGNAL(launchTvReg(QString,QString,bool,bool,QRectF)),this,SLOT(launchTvReg(QString,QString,bool,bool,QRectF)));
    connect(ui->wTvReg,SIGNAL(finishedTvRect(QString)),this,SLOT(finishedTvRect(QString)));
    connect(ui->wLearn,SIGNAL(finishedLearning(QString)),this,SLOT(finishedLearning(QString)));
    connect(ui->menuSettings,SIGNAL(triggered()),this,SLOT(menuSettingsClick()));

    ui->wLearn->setManagerAuth(ui->wWeb->getMannager());
    ui->wTvReg->setManagerAuth(ui->wWeb->getMannager());
}

Wizzard::~Wizzard()
{
    delete ui;
}

void Wizzard::smbConnected(SmbConnection connection)
{
    ui->wSmbConnect->setVisible(false);
    ui->wLearn->setConnection(connection);
    ui->wTvReg->setConnection(connection);
    ui->wWeb->setVisible(true);
}

void Wizzard::testLearn(SmbConnection connection)
{
    ui->wSmbConnect->setVisible(false);
    ui->wLearn->setConnection(connection);
    ui->wTvReg->setConnection(connection);
    ui->wLearn->setVisible(true);
    ui->wLearn->startCapture("2","/OtwGateway/admin/client/whatever","/OtwGateway/admin/client/images/save");
}

void Wizzard::testRect(SmbConnection connection)
{
    ui->wSmbConnect->setVisible(false);
    ui->wTvReg->setConnection(connection);
    ui->wTvReg->setVisible(true);
    ui->wTvReg->startTvReg("/OtwGateway/admin/client/whatever", "/OtwGateway/admin/client/whatever",WTvReg::Mark,QRect(100,50, 100,50));
}

void Wizzard::launchLearn(QString clientId, QString cancelLink, QString postPath)
{
    //qDebug() << clientId << "   " << postPath;
    ui->wWeb->setVisible(false);
    ui->wLearn->setVisible(true);
    ui->wLearn->startCapture(clientId,cancelLink,postPath);
}


void Wizzard::launchTvReg(QString postPath, QString cancelLink, bool capturePic, bool canResize, QRectF rect)
{
    ui->wWeb->setVisible(false);
    ui->wTvReg->setVisible(true);
    WTvReg::TvRegMode mode = WTvReg::Record;
    if(canResize) mode = WTvReg::Mark;
    ui->wTvReg->startTvReg(postPath, cancelLink,mode,rect);
   // ui->wTvReg->startTvReg(postPath,cancelLink,capturePic,canResize,rect);
}

void Wizzard::finishedLearning(QString url)
{
    qDebug() << "finishedLearning goto: " << url;
    ui->wLearn->setVisible(false);
    ui->wWeb->openUrl(url);
    ui->wWeb->setVisible(true);
}

void Wizzard::finishedTvRect(QString url)
{
    qDebug() << "finishedTvRect goto: " << url;
    ui->wTvReg->setVisible(false);
    ui->wWeb->openUrl(url);
    ui->wWeb->setVisible(true);
}

void Wizzard::menuSettingsClick()
{
    Settings dialog(this);
    dialog.exec();

}


