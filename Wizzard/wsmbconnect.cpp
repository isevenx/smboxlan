#include "wsmbconnect.h"
#include "ui_wsmbconnect.h"

WSmbConnect::WSmbConnect(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WSmbConnect)
{
    ui->setupUi(this);
    port = 9898;
    udpGroupAddress = QHostAddress("239.255.39.23");
    udpSocket = new QUdpSocket(this);
    udpSocket->bind(QHostAddress::AnyIPv4, port, QUdpSocket::ShareAddress);
    udpSocket->joinMulticastGroup(udpGroupAddress);
    connect(udpSocket, SIGNAL(readyRead()),this, SLOT(processUdpDatagrams()));
    connect(ui->btnPing, SIGNAL(clicked()),this, SLOT(pingSmb()));
    connect(ui->btnPingL,SIGNAL(clicked()),&lanscanner,SLOT(startScan()));
    connect(&lanscanner,SIGNAL(foundSmartBox(QString)),this,SLOT(foundSmartBox(QString)));
    connect(&lanscanner,SIGNAL(complitedScan(int)),this,SLOT(btnPushPermission(int)));
    connect(ui->btnStart, SIGNAL(clicked()),this, SLOT(startClicked()));
    connect(ui->btnTestLearn, SIGNAL(clicked()),this, SLOT(testLearnClicked()));
    connect(ui->btnTestRect, SIGNAL(clicked()),this, SLOT(testRectClicked()));
    connect(ui->smbIpEdit,SIGNAL(textChanged(QString)),this,SLOT(ipTextChanged(QString)));
    ui->ifaceLabel->setVisible(false);
    ui->ifaceList->setVisible(false);
    refreshIfaces();
    setButtonStates();
    pingSmb();
}

WSmbConnect::~WSmbConnect()
{
    delete ui;
}
void WSmbConnect::btnPushPermission(int res){
   if (res==1) ui->btnPingL->setEnabled(true);
   else ui->btnPingL->setEnabled(false);
}
void WSmbConnect::pingSmb()
{
    ui->smbList->clear();
    QByteArray response = "SMBWIZ";
    udpSocket->writeDatagram(response.data(), response.size(),udpGroupAddress, port);
}
void WSmbConnect::foundSmartBox(QString address){
    ui->smbList->addItem(address);
}

void WSmbConnect::refreshIfaces()
{
    QNetworkConfigurationManager mgr;
    QList<QNetworkConfiguration> activeConfigs = mgr.allConfigurations(QNetworkConfiguration::Active);
    ui->ifaceList->clear();
    for(int i = 0; i < activeConfigs.size(); i++)
    {
        ui->ifaceList->addItem(activeConfigs[i].name(),activeConfigs[i].identifier());
    }
}

void WSmbConnect::startClicked()
{
    if(ui->smbIpEdit->text().length() == 0) return;
    SmbConnection connection;
    connection.setIpPort(ui->smbIpEdit->text());
    emit smbConnected(connection);
}

void WSmbConnect::testLearnClicked()
{
    if(ui->smbIpEdit->text().length() == 0) return;
    SmbConnection connection;
    connection.setIpPort(ui->smbIpEdit->text());
    emit testLearn(connection);
}

void WSmbConnect::testRectClicked()
{
    if(ui->smbIpEdit->text().length() == 0) return;
    SmbConnection connection;
    connection.setIpPort(ui->smbIpEdit->text());
    emit testRect(connection);
}


void WSmbConnect::ifaceChanged()
{

}

void WSmbConnect::smbSelected(QListWidgetItem * item)
{
    ui->smbIpEdit->setText(item->text());
}

void WSmbConnect::processUdpDatagrams()
{
    while (udpSocket->hasPendingDatagrams()) {
        QByteArray request;
        request.resize(udpSocket->pendingDatagramSize());
        udpSocket->readDatagram(request.data(), request.size());

        if(request.startsWith("SMBSERV")){
            QString smbResponse(request);
            smbResponse = smbResponse.mid(7).append(":").append(QString::number(port));
            bool contains = false;
            for(int i = 0; i < ui->smbList->count(); i++){
                if(ui->smbList->item(i)->text() == smbResponse){
                    contains = true;
                    break;
                }
            }
            if(!contains){
                if(ui->smbList->count() == 0 && ui->smbIpEdit->text().isEmpty()) ui->smbIpEdit->setText(smbResponse);
                ui->smbList->addItem(smbResponse);
            }
        }
        qDebug() << tr("Received datagram: \"%1\" ").arg(request.data());
    }
}


void WSmbConnect::ipTextChanged(QString text)
{
    setButtonStates();
}

void WSmbConnect::setButtonStates()
{
    bool enabled = true;
    if(ui->smbIpEdit->text().isEmpty()) enabled = false;

    ui->btnStart->setEnabled(enabled);
    ui->btnTestLearn->setEnabled(enabled);
    ui->btnTestRect->setEnabled(enabled);
}

void WSmbConnect::on_btnPingL_clicked()
{
    ui->smbList->clear();
}
