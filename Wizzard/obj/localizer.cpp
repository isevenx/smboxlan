#include "localizer.h"

Localizer::Localizer(QObject *parent) :
    QObject(parent)
{
    parser = 0;
    init();
}

void Localizer::terminate()
{
    qDebug() << "Localizer::terminate";
    emit finished();
}

void Localizer::load(SmartSettings settings)
{
    this->settings = settings;
    init();
}

void Localizer::init()
{
    if(parser != 0) delete parser;
    parser = new PicParse(settings,true);
}

void Localizer::parsePic(cv::Mat * pic)
{
    parser->ParseFaces(*pic);
    std::vector<cv::Mat> faces;
    if(parser->faceCount()>0){
        faces = parser->GetFaces();
        std::vector<cv::Mat>::iterator max = faces.begin();
        unsigned int area = (*max).rows * (*max).cols;
        unsigned int tArea;

        for(std::vector<cv::Mat>::iterator f = faces.begin()+1; f != faces.end(); ++f){
            tArea = (*f).rows * (*f).cols;
            if(tArea > area){
                area = tArea;
                max = f;
            }
        }

        //double conf = learn.addFace(new cv::Mat(*max));
        //ui->modelProgress->setValue((int)conf);

        emit picParsed(pic, new cv::Mat(*max));
    }else{
        emit picParsed(pic,0);
    }
}
