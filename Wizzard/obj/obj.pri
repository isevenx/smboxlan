INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += \
    $$PWD/smbconnection.h \
    $$PWD/localizer.h \
    $$PWD/learner.h \
    $$PWD/smartwebpage.h \
    $$PWD/smartfacespack.h \
    $$PWD/rectselectscene.h \
    $$PWD/smarttvrecpack.h \
    obj/smartzoomableview.h

SOURCES += \
    $$PWD/smbconnection.cpp \
    $$PWD/localizer.cpp \
    $$PWD/learner.cpp \
    $$PWD/smartwebpage.cpp \
    $$PWD/smartfacespack.cpp \
    $$PWD/rectselectscene.cpp \
    $$PWD/smarttvrecpack.cpp \
    obj/smartzoomableview.cpp

