#ifndef LEARNER_H
#define LEARNER_H

#include <QObject>

#include "facelearn.h"

class Learner : public QObject
{
    Q_OBJECT
public:
    explicit Learner(QObject *parent = 0);
    std::vector<QByteArray *> getGoodFaces();
    void saveToFile(QString filename);
    int facesTrained();
    void clear();

signals:
    void faceAdded(double precision, int facesTrained);
    void finished();
public slots:
    void addFace(cv::Mat * face);
    void terminate();

private:
    FaceLearn learn;
};

#endif // LEARNER_H
