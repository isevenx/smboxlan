#ifndef LOCALIZER_H
#define LOCALIZER_H

#include <QObject>
#include "picparse.h"
#include "settings/smartsettings.h"

class Localizer : public QObject
{
    Q_OBJECT
public:
    explicit Localizer(QObject *parent = 0);
    void load(SmartSettings settings);

signals:
    void picParsed(cv::Mat * pic, cv::Mat * face);
    void finished();

public slots:
    void parsePic(cv::Mat * pic);
    void terminate();

private:
    PicParse * parser;
    SmartSettings settings;

    void init();
};

#endif // LOCALIZER_H
