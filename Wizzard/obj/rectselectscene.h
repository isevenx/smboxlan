#ifndef RECTSELECTSCENE_H
#define RECTSELECTSCENE_H

#include "math.h"

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsPixmapItem>
#include <QImage>
#include <QDebug>


class RectSelectScene : public QGraphicsScene
{
public:

    enum Mode { Idle, MoveRect, ResizeRect };

    RectSelectScene(QObject *parent = 0);

    void setImage(QByteArray image);

    void setRect(QRectF rect, bool force = false);
    inline QRectF getRect(){return rectRegion->rect();}
    inline QPointF getPicRect(){if(imageItem == 0) return QPointF(0,0); return QPointF(imageItem->boundingRect().width(),imageItem->boundingRect().height());}
    inline void setCanResize(bool canResize){this->canResize = canResize;}

public slots:
signals:
    void itemSelected(QGraphicsItem *item);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);

private:
    //bool isItemChange(int type);

    Mode mode;
    QGraphicsPixmapItem * imageItem;
    QGraphicsRectItem * rectRegion;
    QGraphicsRectItem * rectResize;

    QGraphicsItemGroup * background;

    QPointF lastPos;
    QRectF startRect;

    void resizeRect(QPointF mouse);
    void moveRect(QPointF mouse);
    bool canResize;
};

#endif // RECTSELECTSCENE_H
