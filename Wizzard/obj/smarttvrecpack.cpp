#include "smarttvrecpack.h"


SmartTvRecPack::SmartTvRecPack(QNetworkAccessManager * manager, QString postPath, std::vector<QByteArray> tvRectPic, QRectF tvRect, QPointF picRect, QObject *parent) :
    QObject(parent)
{
    this->picRect = picRect;
    aborted = false;
    this->tvRectPic = tvRectPic;
    this->tvRect = tvRect;
    this->postPath = postPath;
    reply = 0;
    multiPart = 0;
    this->manager = manager;
    attempts = 0;
    isSending = false;
}

SmartTvRecPack::~SmartTvRecPack()
{
    qDebug() << "Deleteing SmartTvRecPack data";
}

void SmartTvRecPack::send()
{
    if(isSending) return;
    isSending = true;

    makeRequest();
}

void SmartTvRecPack::makeRequest()
{
    QUrl url(settings.value("Wizzard/serverDomain","http://192.168.4.142:8080").toString().append(postPath));
    qDebug() << "Posting tvRect to: " << url.toString();
    QNetworkRequest request(url);
    request.setRawHeader("User-Agent",settings.value("userAgent","smartAgent").toString().toStdString().c_str());

    QHttpMultiPart * mp  = getMultipart();
    qDebug() << request.url();
    reply = manager->post(request, mp);
    reply->setParent(this);

    connect(reply, SIGNAL(finished()), this, SLOT(nrFinished()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(nrError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(nrSslErrors(QList<QSslError>)));
}

QHttpMultiPart * SmartTvRecPack::getMultipart()
{
    if(multiPart != 0){
        delete multiPart;
    }

    QHttpPart textPart;
    QHttpPart imagePart;

    multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    multiPart->setParent(this);

    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"rect.x\""));
    textPart.setBody(QString::number(tvRect.x()).toUtf8());
    multiPart->append(textPart);
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"rect.y\""));
    textPart.setBody(QString::number(tvRect.y()).toUtf8());
    multiPart->append(textPart);
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"rect.width\""));
    textPart.setBody(QString::number(tvRect.width()).toUtf8());
    multiPart->append(textPart);
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"rect.height\""));
    textPart.setBody(QString::number(tvRect.height()).toUtf8());
    multiPart->append(textPart);

    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"width\""));
    textPart.setBody(QString::number(picRect.x()).toUtf8());
    multiPart->append(textPart);
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"height\""));
    textPart.setBody(QString::number(picRect.y()).toUtf8());
    multiPart->append(textPart);

    if(!tvRectPic.empty()){
        int fri = 0;
        for(std::vector<QByteArray>::iterator fr = tvRectPic.begin(); fr != tvRectPic.end(); ++fr, fri++){
            imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
            imagePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                QVariant(QString("form-data; name=\"images[%1]\"; filename=\"image.jpg\"").arg(fri)));
            imagePart.setBody(*fr);
            multiPart->append(imagePart);
        }
    }

    return multiPart;
}

void SmartTvRecPack::nrError(QNetworkReply::NetworkError code __attribute__ ((unused)))
{
    qDebug() << "Network error: " << reply->errorString();
}

void SmartTvRecPack::nrSslErrors(const QList<QSslError> & errors __attribute__ ((unused)))
{
    qDebug() << "SSL ERROR";
    reply->ignoreSslErrors();
}

void SmartTvRecPack::nrFinished()
{
    if(!aborted){
        if(reply->error() != QNetworkReply::NoError){
            if(++attempts > settings.value("SmartFacesPack/maxAttemts",3).toInt()){
                qDebug() << "Network request FAIL, maxAttempts exeeded DROPING DATA";
                abort();
            }else{
                qDebug() << "Network request FAIL retrying";
                QTimer::singleShot(settings.value("SmartFacesPack/retryAfterMs",5000).toInt(),this,SLOT(makeRequest()));
            }
        }else{
            qDebug() << "Network request successful";
            done();
        }
    }
}

void SmartTvRecPack::abort()
{
    static const QMetaMethod failedSignal = QMetaMethod::fromSignal(&SmartTvRecPack::failed);
    if (this->isSignalConnected(failedSignal)) {
        emit failed(this);
    }else{
        deleteLater();
    }
}

void SmartTvRecPack::done()
{
    QByteArray bytes = reply->readAll();
    QJsonDocument doc = QJsonDocument::fromJson(bytes);
    qDebug() << "Supplied redirect link: "<< bytes;

    QJsonObject jsonObj = doc.object();
    redirectLink = jsonObj.value("redirectLink").toString();

    static const QMetaMethod finishedSignal = QMetaMethod::fromSignal(&SmartTvRecPack::finished);
    if (this->isSignalConnected(finishedSignal)) {
        emit finished(this);
    }else{
        deleteLater();
    }
}

QString SmartTvRecPack::getRedirectLink()
{
    if(redirectLink.count() == 0) return settings.value("Wizzard/serverDomain","http://192.168.4.142:8080").toString()
            .append(settings.value("Wizzard/startPath","/OtwGateway/wizard/device/registration/residence").toString());
    return redirectLink;
}

void SmartTvRecPack::stop()
{
    aborted = true;
    if(reply != 0 && !reply->isFinished()) reply->abort();
}
