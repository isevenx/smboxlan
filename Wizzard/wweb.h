#ifndef WWEB_H
#define WWEB_H

#include <QWidget>
#include <QDebug>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkCookieJar>
#include <QNetworkAccessManager>


#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include <QJsonDocument>
#include <QWebFrame>
#include <QSettings>
#include <QPalette>

#include "smartwebpage.h"

namespace Ui {
class WWeb;
}

class WWeb : public QWidget
{
    Q_OBJECT

signals:
    void launchLearn(QString clientId, QString cancelLink, QString postPath);
    void launchTvReg(QString postPath, QString cancelLink, bool capturePic, bool canResize, QRectF rect);

public:
    explicit WWeb(QWidget *parent = 0);
    ~WWeb();

    inline QNetworkAccessManager * getMannager(){return manager;}

public slots:
    void openUrl(QString url);

private:
    Ui::WWeb *ui;
    SmartWebPage * webPage;
    QNetworkAccessManager * manager;
    QSettings settings;

private slots:
    void loadFinished(bool ok);
};

#endif // WWEB_H
