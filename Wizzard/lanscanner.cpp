#include "lanscanner.h"
#include <QDebug>
#include <QPointer>
 QPointer<QReplyTimeout> timeout;
 int replyCount=0;
 int CRC=0; //complitedReplyCount
bool LanScanner::ifSmbox(QByteArray str)
{
    if (str.startsWith("SmBox")) return true;
    else return false;
}
void LanScanner::complitedScanFunc(){
    int result;
    if ( CRC==replyCount ) result = 1;
    else result = 0;
    emit complitedScan(result);
}
void LanScanner::getRequest(QNetworkRequest request){
    reply=manager->get(request);
    connect(reply, SIGNAL(finished()),this, SLOT(onRequestCompleted()));
}
void LanScanner::startScan(){
    emit complitedScan(0);
    counter = 0;
    replyCount = 0;CRC = 0;
    QString IP,Netmask,IP_max,IP_min,bin;
    int q=0;
    QList<QNetworkInterface> ifaces = QNetworkInterface::allInterfaces();
    QNetworkInterface iface = ifaces.at(1);     //Network(0 - local;1 - current)
    IP=iface.addressEntries().at(0).ip().toString();
    Netmask=iface.addressEntries().at(0).netmask().toString();
//----------------------------------------------------------------------------------------
    //Netmask = "255.255.0.0";        // exclusive netmask
    IP_min=minMaxIP(IP,Netmask,0);
    IP_max=minMaxIP(IP,Netmask,1);
//----------------------------------------------------------------------------------------
    IP_min.append('.');
    IP_max.append('.');
    for (int i=0;i<IP_min.length();i++){
        if (IP_min[i]=='.'){
            min[q]=bin.toInt();
            q++;
            bin="";
        }
        else bin.append(IP_min[i]);
    }
    q=0;
    for (int i=0;i<IP_max.length();i++){
        if (IP_max[i]=='.'){
            max[q]=bin.toInt(); max_D[q]=bin.toInt();
            q++;
            bin="";
        }
        else bin.append(IP_max[i]);
    }
    q=0;
    IP_min.chop(1);
    IP_max.chop(1);
//----------------------------------------------------------------------------------------
    delete manager;
    manager = new QNetworkAccessManager(this);   
    for(int i=0;i<4;i++)min_D[i]=min[i];    
    BigSender();
}
void LanScanner::onRequestCompleted()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    QByteArray str;

    str=reply->readAll();
    if (ifSmbox(str)) {
        str.remove(0,5);
        str.append(":9898");
        emit foundSmartBox(str);
    }

    counter ++;
    replyCount ++;
    //qDebug() << replyCount;
    if (counter==max_D[3]-min_D[3]+1) {
        delete timeout;
        timeout = new QReplyTimeout(reply, 1);
        counter = 0;
        if ((min_D[2]<max_D[2]) ){
                min_D[2]++;
                BigSender();
             }
        if ((min_D[1]<max_D[1])&&(min_D[2]==max_D[2])){
                min_D[1]++;                
                BigSender();
            }
    }
    complitedScanFunc();
    reply->deleteLater();
}
void LanScanner::BigSender(){
    QString url;   

    if ((min_D[2]==max_D[2])&&(min_D[1]==max_D[1])) max_D[3]=max[3];
    else max_D[3]=255;
    if (min_D[2]==min[2]) min_D[3]=min[3];
    else min_D[3]=0;
    if (min_D[1]==max_D[1]) max_D[2]=max[2];
    else max_D[2]=255;

    for (int changer = min_D[3];changer<=max_D[3];changer++){
        CRC++;
        url="http://";
        url=url+QString::number(min_D[0])+'.'+QString::number(min_D[1])+'.'+QString::number(min_D[2])+'.'+QString::number(changer);
        url=url+":9898/scanid";
        QNetworkRequest request = QNetworkRequest(QUrl(url));
        qDebug() << QString::number(min_D[0])<<'.'<<QString::number(min_D[1])<<'.'<<QString::number(min_D[2])<<'.'<<QString::number(changer);
        getRequest(request);
    }    
}
QString LanScanner::minMaxIP(QString IP, QString Netmask,int minmax)
{
    QString bin="",result="";
    int IP_part[4],Netmask_part[4],q=0;
    unsigned int I_IP,I_Netmask;

    IP.append('.');
    for (int i=0;i<IP.length();i++){
        if (IP[i]=='.'){
            IP_part[q]=bin.toInt();
            q++;
            bin="";
        }
        else bin.append(IP[i]);
    }
    IP.chop(1);
    q=0;
    Netmask.append('.');
    for (int i=0;i<Netmask.length();i++){
        if (Netmask[i]=='.'){
            Netmask_part[q]=bin.toInt();
            q++;
            bin="";
        }
        else bin.append(Netmask[i]);
    }
    Netmask.chop(1);
//----------------------------------------------------------------------------------------
    I_IP = IP_part[3] | (IP_part[2]<<  8)  | (IP_part[1] << 16 ) | (IP_part[0] << 24 );
    I_Netmask = Netmask_part[3] | (Netmask_part[2] << 8) | (Netmask_part[1] << 16 ) | (Netmask_part[0] << 24 );

    if (minmax==0) I_IP = I_IP & I_Netmask;
    else {
        I_Netmask = ~I_Netmask;
        I_IP = I_IP | I_Netmask;
    }
    IP_part[0]=I_IP >> 24 ;
    IP_part[1]=(I_IP >> 16 )&255;
    IP_part[2]=(I_IP >> 8)&255;
    IP_part[3]=(I_IP & 255);
    result=result+QString::number(IP_part[0])+'.'+QString::number(IP_part[1])+'.'+QString::number(IP_part[2])+'.'+QString::number(IP_part[3]);
    return result;
}
LanScanner::LanScanner(QObject *parent) :
    QObject(parent)
{
}

