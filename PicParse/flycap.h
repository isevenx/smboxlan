#ifndef FLYCAP_H
#define FLYCAP_H

#include "flycapture/FlyCapture2.h"

#include "opencv2/imgproc/imgproc.hpp"
#include <QDebug>

#define SOFTWARE_TRIGGER   0x62C

class FlyCap
{
public:
    FlyCap();
    ~FlyCap();
    bool tryConnect();
    bool ok();
    void disconnect();
    bool prepareFrame();
    cv::Mat * getFrame();
private:
    FlyCapture2::Camera camera;
    FlyCapture2::CameraInfo camInfo;
    bool CheckTriggerReady();
    bool CheckSoftwareTriggerPresence();
    unsigned int cameraID;

};

#endif // FLYCAP_H
