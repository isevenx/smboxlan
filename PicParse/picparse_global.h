#ifndef PICPARSE_GLOBAL_H
#define PICPARSE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(PICPARSE_LIBRARY)
#  define PICPARSESHARED_EXPORT Q_DECL_EXPORT
#else
#  define PICPARSESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // PICPARSE_GLOBAL_H
