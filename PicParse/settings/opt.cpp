#include "opt.h"

Opt::Opt()
{
}

bool Opt::dumpToFile(QString filename)
{

    //if(!QFile::exists(filename)){
    QFile file(filename);
    if(!file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate)){
        qCritical() << "Opt: Failed to open output file [" << filename << "] for saving JSON!";
        return false;
    };
    QJsonDocument doc(getJSON().toObject());
    file.write(doc.toJson());
    file.close();
    return true;
}

bool Opt::readFromFile(QString filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "Failed to read json from file "<< filename;
        return false;
    }

    QByteArray json;
    json = file.readAll();
    QJsonDocument doc = QJsonDocument::fromJson(json);
    return setJSON(doc.object());
}

QByteArray Opt::toByteArray()
{
    QJsonDocument doc(getJSON().toObject());
    return doc.toJson();
}

bool Opt::fromByteArray(QByteArray data)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);
    return setJSON(doc.object());
}

// ========================================================================= OPT

bool Opt::getOpt(QJsonObject src, Opt &value, QString fieldName, bool required, QString invalidMsg){
    bool set;
    return getOpt(src,value,fieldName,required,set,invalidMsg);
}

bool Opt::getOpt(QJsonObject src, Opt &value, QString fieldName, bool required, bool &set, QString invalidMsg)
{
    set = contains(src,fieldName,required);
    if(!set) return !required;

    if(!src[fieldName].isObject()){
        qWarning() << QString(invalidMsg).arg(fieldName).toStdString().c_str();
        return false;
    }
    return value.setJSON(src[fieldName]);
}

// ========================================================================= INT

bool Opt::getInt(QJsonObject src, int &value, QString fieldName, bool required, QString invalidMsg)
{
    bool set;
    return getInt(src,value,fieldName,required,set,invalidMsg);
}

bool Opt::getInt(QJsonObject src, int &value, QString fieldName, bool required, bool &set, QString invalidMsg)
{
    set = contains(src,fieldName,required);
    if(!set) return !required;

    if(!src[fieldName].isDouble()){
        qWarning() << QString(invalidMsg).arg(fieldName).toStdString().c_str();
        return false;
    }
    value = (int)src[fieldName].toDouble();
    return true;
}

// ========================================================================= DOUBLE

bool Opt::getDouble(QJsonObject src, double &value, QString fieldName, bool required, QString invalidMsg)
{
    bool set;
    return getDouble(src,value,fieldName,required,set,invalidMsg);
}


bool Opt::getDouble(QJsonObject src, double &value, QString fieldName, bool required, bool &set, QString invalidMsg)
{
    set = contains(src,fieldName,required);
    if(!set) return !required;

    if(!src[fieldName].isDouble()){
        qWarning() << QString(invalidMsg).arg(fieldName).toStdString().c_str();
        return false;
    }
    value = src[fieldName].toDouble();
    return true;
}

// ========================================================================= STRING

bool Opt::getString(QJsonObject src, QString &value, QString fieldName, bool required, QString invalidMsg)
{
    bool set;
    return getString(src,value,fieldName,required,set,invalidMsg);
}


bool Opt::getString(QJsonObject src, QString &value, QString fieldName, bool required, bool &set, QString invalidMsg)
{
    set = contains(src,fieldName,required);
    if(!set) return !required;

    if(!src[fieldName].isString()){
        qWarning() << QString(invalidMsg).arg(fieldName).toStdString().c_str();
        return false;
    }
    value = src[fieldName].toString();
    return true;
}

// ========================================================================= OBJECT


bool Opt::getObject(QJsonObject src, QJsonObject &value, QString fieldName, bool required, QString invalidMsg)
{
    bool set;
    return getObject(src,value,fieldName,required,set,invalidMsg);
}

bool Opt::getObject(QJsonObject src, QJsonObject &value, QString fieldName, bool required, bool &set, QString invalidMsg)
{
    set = contains(src,fieldName,required);
    if(!set) return !required;

    if(!src[fieldName].isObject()){
        qWarning() << QString(invalidMsg).arg(fieldName).toStdString().c_str();
        return false;
    }
    value = src[fieldName].toObject();
    return true;
}

// ========================================================================= ARRAY

bool Opt::getArray(QJsonObject src, QJsonArray &value, QString fieldName, bool required, QString invalidMsg)
{
    bool set;
    return getArray(src,value,fieldName,required,set,invalidMsg);
}

bool Opt::getArray(QJsonObject src, QJsonArray &value, QString fieldName, bool required, bool &set, QString invalidMsg)
{
    set = contains(src,fieldName,required);
    if(!set) return !required;

    if(!src[fieldName].isArray()){
        qWarning() << QString(invalidMsg).arg(fieldName).toStdString().c_str();
        return false;
    }
    value = src[fieldName].toArray();
    return true;
}

// ========================================================================= BOOL


bool Opt::getBool(QJsonObject src, bool &value, QString fieldName, bool required, QString invalidMsg)
{
    bool set;
    return getBool(src,value,fieldName,required,set,invalidMsg);
}

bool Opt::getBool(QJsonObject src, bool &value, QString fieldName, bool required, bool &set, QString invalidMsg)
{
    set = contains(src,fieldName,required);
    if(!set) return !required;

    if(!src[fieldName].isDouble() && !src[fieldName].isString()){
        qWarning() << QString(invalidMsg).arg(fieldName).toStdString().c_str();
        return false;
    }
    if(src[fieldName].isDouble()){
        if(src[fieldName].toDouble() == 1){
            value = true;
        }else{
            value = false;
        }
    }else if(src[fieldName].isString()){
        QString str = src[fieldName].toString();
        if(str == "YES"||str == "yes"||str == "TRUE"||str == "true"){
            value = true;
        }else{
            value = false;
        }
    }
    return true;
}

// ========================================================================= ------

bool Opt::contains(QJsonObject src, QString fieldName, bool required, QString requiredMsg)
{
    if(!src.contains(fieldName)){
        if(required){
            qWarning() << QString(requiredMsg).arg(fieldName).toStdString().c_str();
        }
        return false;
    }
    return true;
}
