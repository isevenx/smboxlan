#ifndef REGIONSETTINGS_H
#define REGIONSETTINGS_H

#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>

#include "opt.h"
#include "optrect.h"


class RegionSettings : public Opt
{
public:
    RegionSettings();

    OptRect channelRect;
    OptRect unifiedRect;


    bool setJSON(QJsonValue value);
    QJsonValue getJSON();
    void setDefaults();

    bool operator==(RegionSettings &other);
    inline bool operator!=(RegionSettings &other){return !(*this == other);};
};

#endif // REGIONSETTINGS_H
