#ifndef SMARTSETTINGS_H
#define SMARTSETTINGS_H

#include <QFile>
#include <QJsonObject>
#include <QJsonDocument>

#include "optcapsource.h"
#include "optprocessor.h"
#include "optsender.h"

class SmartSettings :public Opt
{
public:
    explicit SmartSettings();
    virtual ~SmartSettings();
    SmartSettings(QString filename);

    int grabTimer;
    bool enableWebui;
    OptCapSource tvSource;
    OptCapSource wcSource;
    OptProcessor processor;
    OptSender sender;
    QString serverUrl;
    int rectUpdateIntervalS;

    bool setJSON(QJsonValue value);
    QJsonValue getJSON();
    void setDefaults();

    bool operator==(SmartSettings &other);
    inline bool operator!=(SmartSettings &other){return !(*this == other);}
};

#endif // SMARTSETTINGS_H
