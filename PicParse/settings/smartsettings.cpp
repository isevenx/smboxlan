#include "smartsettings.h"

SmartSettings::SmartSettings()
{
    setDefaults();
}

SmartSettings::~SmartSettings()
{
}

SmartSettings::SmartSettings(QString filename)
{
    if(!QFile::exists(filename)){
        setDefaults();
        dumpToFile(filename);
    }else{
        readFromFile(filename);
    }
}

void SmartSettings::setDefaults()
{
    enableWebui = true;
    tvSource.sourceType = EASY_CAP;
    tvSource.sourceNum = 0;
    wcSource.sourceType = FLYCAP_CAP;
    wcSource.sourceNum = 0;
/*
    tvSource.sourceType = VIDEO_CAP;
    tvSource.filename = "/home/rotjix/data/test.avi";
    wcSource.sourceType = VIDEO_CAP;
    wcSource.filename = "/home/rotjix/data/test.avi";
*/
    serverUrl = "http://192.168.4.142:8080";
    grabTimer = 1000;
    processor = OptProcessor();
    sender = OptSender();
    rectUpdateIntervalS = 10;
}

bool SmartSettings::setJSON(QJsonValue value)
{
    QJsonObject tObj;
    bool set;

    if(!value.isObject()){
        qWarning("Opt: Invalid options - Must be JSON object");
        return false;
    }
    QJsonObject opt = value.toObject();

    //=============================================================== server_url
    if(!getString(opt,serverUrl,"server_url",true)) return false;
    //=============================================================== rect_update_interval_s
    if(!getInt(opt,rectUpdateIntervalS,"rect_update_interval_s",true)) return false;
    //=============================================================== tv_source
    if(!getObject(opt,tObj,"tv_source",true)) return false;
    if(!tvSource.setJSON(tObj)){
        qWarning("Opt: Failed to read tv_source settings");
        return false;
    };
    //=============================================================== wc_source
    if(!getObject(opt,tObj,"wc_source",true)) return false;
    if(!wcSource.setJSON(tObj)){
        qWarning("Opt: Failed to read wc_source settings");
        return false;
    };
    //=============================================================== grab_timer
    if(!getInt(opt,grabTimer,"grab_timer")) return false;
    //=============================================================== processor
    if(!getObject(opt,tObj,"processor",false,set)) return false;
    if(set){
        processor = OptProcessor();
        processor.setJSON(tObj);
    }
    //=============================================================== sender
    if(!getObject(opt,tObj,"sender",false,set)) return false;
    if(set){
        sender = OptSender();
        sender.setJSON(tObj);
    }
    //=============================================================== enable_webui
    if(!getBool(opt,enableWebui,"enable_webui")) return false;
    return true;
}

QJsonValue SmartSettings::getJSON()
{
    QJsonObject tObj;

    tObj.insert("server_url",serverUrl);
    tObj.insert("rect_update_interval_s",(double)rectUpdateIntervalS);
    tObj.insert("tv_source",tvSource.getJSON());
    tObj.insert("wc_source",wcSource.getJSON());
    tObj.insert("grab_timer",grabTimer);
    tObj.insert("processor",processor.getJSON());
    tObj.insert("sender",sender.getJSON());
    tObj.insert("enable_webui",QString(enableWebui?"true":"false"));
    return QJsonValue(tObj);
}

bool SmartSettings::operator==(SmartSettings &other){
    if(serverUrl != other.serverUrl) return false;
    if(rectUpdateIntervalS != other.rectUpdateIntervalS) return false;
    if(wcSource != other.wcSource) return false;
    if(tvSource != other.tvSource) return false;
    if(grabTimer != other.grabTimer) return false;
    if(processor != other.processor) return false;
    if(sender != other.sender) return false;
    if(enableWebui != other.enableWebui) return false;
    return true;
}
