#ifndef PICPARSE_H
#define PICPARSE_H

#include "picparse_global.h"

#include <vector>
#include <cmath>
#include <algorithm>
#include <stdexcept>

#include <QString>
#include <QResource>
#include <QBuffer>
#include <QDebug>
#include <QDir>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "jpeglib.h"

#include "parsedfaces.h"

// For development only
#include <cstdio>
#include <iostream>

#include "smartconv.h"
#include "settings/smartsettings.h"

class ParseCascade
{
public:
    int flags;
    cv::Size minSize;
    cv::Size maxSize;
    bool hasMaxSize;
    QString path;
    double scaleStep;
    int minNeighbors;
    cv::CascadeClassifier classifier;

    inline bool fromOpt(OptCascade tCascade){
        if(!classifier.load(tCascade.path.toStdString()))
        {
            qWarning() << "Cascade file "<< tCascade.path <<" not found";
            return false;
        }
        flags = tCascade.flags;
        hasMaxSize = tCascade.hasMaxSize;
        maxSize = tCascade.maxSize;
        minNeighbors = tCascade.minNeighbors;
        minSize = tCascade.minSize;
        path = tCascade.path;
        scaleStep = tCascade.scaleStep;
        return true;
    }
};

class ParseRotation
{
public:
    int maxAngle;
    int angleStep;
    std::vector<ParseCascade> cascades;
};

class PICPARSESHARED_EXPORT PicParse
{

public:
    PicParse(SmartSettings settings = SmartSettings());
    PicParse(SmartSettings settings, bool debug);
    ~PicParse();
    void ParseFaces(cv::Mat &img);
    cv::Mat * getPart(cv::Mat &img, cv::Rect rect);
    QByteArray * getPartB(cv::Mat &img, cv::Rect rect);
    QByteArray * getTvPart(cv::Mat &img);
    QByteArray * getTvPartUnif(cv::Mat &img);
    QByteArray * getScaledTv(cv::Mat &img, QByteArray * orig);
    QByteArray * getScaledWc(cv::Mat &img, QByteArray * orig);

    void DumpFaces(QDir dir, QString filename);
    std::vector<cv::Mat> GetFaces();
    std::vector<QByteArray*> GetFacesJpg();
    //static QByteArray *MatToJpg(cv::Mat src);
    int faceCount();

    static QByteArray * getPartJpeg(QByteArray * source,int x, int y, int w, int h);

private:
    void init();
    SmartSettings settings;
    bool debug;

    // Resulting faces
    cv::Mat *flip_mat; //!< cv::Mat for frame rotation 90 degrees;
    cv::Size flip_mat_size;
    cv::Point flip_mat_center;
    cv::Mat *rot_mat; //!< cv::Mat for frame rotation;
    cv::Size rot_mat_size;
    cv::Scalar flip_rot_mat_def_contents;
    cv::Point rot_mat_center;

    ParsedFaces faces;

    //void InitClassifiers();
    void RotateFrame(cv::Mat src, cv::Mat &dst, double angle);
    cv::Rect ContainedRotatedRect(cv::Rect source, cv::Point center, double angle);
    cv::Point RotatePoint(cv::Point center, cv::Point point, double angle);

    bool initSettings(SmartSettings settings);
    std::vector<ParseRotation> rotations;
    std::vector<ParseCascade> cascadesPostproces;

};

#endif // PICPARSE_H
