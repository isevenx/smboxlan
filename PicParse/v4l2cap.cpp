#include "v4l2cap.h"

V4L2Cap::V4L2Cap(int mDevId)
{
    io = IO_METHOD_MMAP;
    autoReload = false;
    anyDevice = false;
    if(mDevId == -1){
        anyDevice = true;
        mDevId = 0;
        fprintf(stderr, "anyDevice = true \n");
    };
    if(mDevId > 64) mDevId = 0;
    this->devId = mDevId;
    fd = -1;
    width = 640;
    height = 480;
    retryTimeout = 3000;
    grayscale = 0;
    buffers = 0;
    mustDealocate = false;
    dev_name = (char*)malloc(  sizeof( char ) * 50 );
    error = "";
    pixelformat = V4L2_PIX_FMT_YUYV;
    reload();
}

V4L2Cap::~V4L2Cap()
{
    free(dev_name);
    release();
}

int V4L2Cap::xioctl(int fh, int request, void *arg)
{
    int r;
    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

bool V4L2Cap::convertYuvy2Y(const void *src, int maxSize)
{

    if(grayscale == 0){
        grayscale = (char*)malloc(sizeof(char) * ( width*height*1 ));
    }

    if(src == 0) return false;

    int x, y;
    char *Y, *gray;
    //get only Y component for grayscale from (Y1)(U1,2)(Y2)(V1,2)
    if(pixelformat == V4L2_PIX_FMT_YUYV){
        for (y = 0; y < height; y++) {
            Y =(char*) src + (width * 2 * y);
            if(maxSize < width * 2 * y) continue;
            gray = grayscale + (width * y);
            for (x=0; x < width; x += 2) {
                gray[x] = *(Y);
                Y += 2;
                gray[x + 1] = *(Y);
                Y += 2;
            }
        }
    }else if(pixelformat == V4L2_PIX_FMT_UYVY){
        for (y = 0; y < height; y++) {
            Y =(char*) src + (width * 2 * y);
            if(maxSize < width * 2 * y) continue;
            gray = grayscale + (width * y);
            for (x=0; x < width; x += 2) {
                gray[x] = *(Y+1);
                Y += 2;
                gray[x + 1] = *(Y+1);
                Y += 2;
            }
        }
    }

    unsigned char *a, *b, *c;
    for (y = 0; y < height-2; y+=2) {
        a = (unsigned char *)(grayscale + (width * y));
        b = (unsigned char *)(grayscale + (width * (y+1)));
        c = (unsigned char *)(grayscale + (width * (y+2)));
        for (x=0; x < width; x += 1) {
            b[x] = ((unsigned int)a[x]+(unsigned int)c[x])/2;
        }
    }

    return true;
}

char * V4L2Cap::grab()
{
    if(!ok) return 0;
    for (;;) {
        fd_set fds;
        struct timeval tv;
        int r;

        FD_ZERO(&fds);
        FD_SET(fd, &fds);

        /* Timeout. */
        tv.tv_sec = 2;
        tv.tv_usec = 0;

        r = select(fd + 1, &fds, NULL, NULL, &tv);

        if (-1 == r) {
            if (EINTR == errno)
                continue;
            err("select");
            return 0;
        }

        if (0 == r) {
            err("select timeout");
            return 0;
        }

        if (getFrame()){
            return grayscale;
        }

        if(!ok) return 0;
        /* EAGAIN - continue select loop. */
    }

    return grayscale;
}

bool V4L2Cap::getFrame()
{
    struct v4l2_buffer buf;
    unsigned int i;
    bool goodFrame = true;

    switch (io) {
    case IO_METHOD_READ:
        if (-1 == read(fd, buffers[0].start, buffers[0].length)) {
            switch (errno) {
            case EAGAIN: return false;
            case EIO:
                /* Could ignore EIO, see spec. */
                /* fall through */
            default:
                return err("read");
            }
        }

        goodFrame = convertYuvy2Y(buffers[0].start, buffers[0].length);

        break;
    case IO_METHOD_MMAP:
        CLEAR(buf);

        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;

        if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
            switch (errno) {
            case EAGAIN:
                return false;
            case EIO:
                /* Could ignore EIO, see spec. */
                /* fall through */

            default:
                return err("VIDIOC_DQBUF");
            }
        }

        assert(buf.index < n_buffers);

        goodFrame = convertYuvy2Y(buffers[buf.index].start, buf.bytesused);

        if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
            return err("VIDIOC_QBUF");

        break;
    case IO_METHOD_USERPTR:
        CLEAR(buf);

        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_USERPTR;

        if (-1 == xioctl(fd, VIDIOC_DQBUF, &buf)) {
            switch (errno) {
            case EAGAIN:
                return 0;

            case EIO:
                /* Could ignore EIO, see spec. */
                /* fall through */
            default:
                err("VIDIOC_DQBUF");
                return 0;
            }
        }

        for (i = 0; i < n_buffers; ++i)
            if (buf.m.userptr == (unsigned long)buffers[i].start
                    && buf.length == buffers[i].length)
                break;

        assert(i < n_buffers);

        goodFrame = convertYuvy2Y((void *)buf.m.userptr, buf.bytesused);

        if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
            return err("VIDIOC_QBUF");
        break;
    }

    return goodFrame;
}


bool V4L2Cap::reload()
{
    if(anyDevice){
        if(devId >= 64) devId = 0;
        ok = false;
        while(!ok && devId <= 64){
            ok = init();
            devId++;
        }
        return ok;
    }else{
        ok = init();
        return ok;
    }
}

void V4L2Cap::release()
{
    if(grayscale != 0) free(grayscale);
    grayscale = 0;
    if(mustDealocate) {
        unsigned int i;

        switch (io) {
        case IO_METHOD_READ:
                free(buffers[0].start);
                break;

        case IO_METHOD_MMAP:
                for (i = 0; i < n_buffers; ++i)
                        if (-1 == munmap(buffers[i].start, buffers[i].length)){
                            // ERR
                        }
                break;

        case IO_METHOD_USERPTR:
                for (i = 0; i < n_buffers; ++i)
                        free(buffers[i].start);
                break;
        }
        free(buffers);
    }
    buffers = 0;
    mustDealocate = false;
    if (-1 != fd) {
        if(io == IO_METHOD_MMAP || io == IO_METHOD_USERPTR){
            struct v4l2_requestbuffers req;
            CLEAR(req);
            req.count = 0;
            req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            req.memory = V4L2_MEMORY_MMAP;
            if (-1 == ioctl (fd, VIDIOC_REQBUFS, &req)){
                qDebug() << "Unable to release v4l2 buffers";
            }

        }
        close(fd);
        fd = -1;
    }
}



/// INIT

bool V4L2Cap::initRead(unsigned int buffer_size)
{
    mustDealocate = true;
    buffers = (buffer*)calloc(1, sizeof(*buffers));

    if (!buffers) {
        return err("Out of memory");
    }

    buffers[0].length = buffer_size;
    buffers[0].start = malloc(buffer_size);

    if (!buffers[0].start) {
        return err("Out of memory");
    }
    return true;
}

bool V4L2Cap::initMmap()
{
    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count = 4;
    req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            return err("Device does not support memory mapping");
        } else {
            return err("VIDIOC_REQBUFS");
        }
    }

    if (req.count < 2) {
        return err("Insufficient buffer memory");
    }

    mustDealocate = true;
    buffers = (buffer*)calloc(req.count, sizeof(*buffers));

    if (!buffers) {
        return err("Out of memory");
    }

    for (n_buffers = 0; n_buffers < req.count; ++n_buffers) {
        struct v4l2_buffer buf;

        CLEAR(buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = n_buffers;

        if (-1 == xioctl(fd, VIDIOC_QUERYBUF, &buf))
            return err("VIDIOC_QUERYBUF");

        buffers[n_buffers].length = buf.length;
        buffers[n_buffers].start =
                mmap(NULL /* start anywhere */,
                     buf.length,
                     PROT_READ | PROT_WRITE /* required */,
                     MAP_SHARED /* recommended */,
                     fd, buf.m.offset);

        if (MAP_FAILED == buffers[n_buffers].start)
            return err("mmap");
    }
    return true;
}

bool V4L2Cap::initUserp(unsigned int buffer_size)
{
    struct v4l2_requestbuffers req;

    CLEAR(req);

    req.count  = 4;
    req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_USERPTR;

    if (-1 == xioctl(fd, VIDIOC_REQBUFS, &req)) {
        if (EINVAL == errno) {
            return err("Device does not support user pointer i/o");
        } else {
            return err("VIDIOC_REQBUFS");
        }
    }

    mustDealocate = true;
    buffers = (buffer*)calloc(4, sizeof(*buffers));

    if (!buffers) {
        return err("Out of memory");
    }

    for (n_buffers = 0; n_buffers < 4; ++n_buffers) {
        buffers[n_buffers].length = buffer_size;
        buffers[n_buffers].start = malloc(buffer_size);

        if (!buffers[n_buffers].start) {
            return err("Out of memory");
        }
    }
    return true;
}

bool V4L2Cap::init()
{
    ok = false;
    release();

    snprintf(dev_name,50, "/dev/video%d", devId);
    fprintf(stderr, "OPENING %d \n",devId);
    fprintf(stderr, "OPENING %s \n",dev_name);

    struct stat st;

    if (-1 == stat(dev_name, &st)) {
        return err("Cannot identify device");
    }

    if (!S_ISCHR(st.st_mode)) {
        return err("Device is no device");
    }

    fd = open(dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);

    if (-1 == fd) {
        return err("Cannot open v4l2 device");
    }

    struct v4l2_capability cap;
    struct v4l2_format fmt;
    unsigned int min;

    if (-1 == xioctl(fd, VIDIOC_QUERYCAP, &cap)) {
        if (EINVAL == errno) {
            return err("Device is not a v4l2 device");
        } else {
            return err("VIDIOC_QUERYCAP");
        }
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
        return err("Device is not a capture device");
    }

    switch (io) {
    case IO_METHOD_READ:
        if (!(cap.capabilities & V4L2_CAP_READWRITE)) {
            return err("Device does not support read i/o");
            exit(EXIT_FAILURE);
        }
        break;

    case IO_METHOD_MMAP:
    case IO_METHOD_USERPTR:
        if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
            return err("Device oes not support streaming i/o");
        }
        break;
    }

    ////////////////////////////////////////////////
    int index = 0;
    if (-1 == ioctl (fd, VIDIOC_S_INPUT, &index)) {
        //return err("VIDIOC_S_INPUT");
    }

    struct v4l2_input input;
    v4l2_std_id std_id;

    memset (&input, 0, sizeof (input));

    if (-1 == ioctl (fd, VIDIOC_G_INPUT, &input.index)) {
        return err("VIDIOC_G_INPUT");
    }

    if (-1 == ioctl (fd, VIDIOC_ENUMINPUT, &input)) {
        return err("VIDIOC_ENUM_INPUT");
    }

    bool got_std = true;
    if (0 == (input.std & V4L2_STD_PAL)) {
        if (0 == (input.std & V4L2_STD_MTS)) {
            if (0 == (input.std & V4L2_STD_PAL_60)) {
                got_std = false;
//                return err("Oops. Video standart is not supported.");
            }else{
                std_id = V4L2_STD_PAL_60;
            }
        }else{
            std_id = V4L2_STD_MTS;
        }
    }else{
        std_id = V4L2_STD_PAL;
    }

    if (got_std && -1 == ioctl (fd, VIDIOC_S_STD, &std_id)) {
        return err("VIDIOC_S_STD");
    }

    ////////////////////////////////////////////

    getFmt();

    CLEAR(fmt);

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    fmt.fmt.pix.width       = width;
    fmt.fmt.pix.height      = height;
    fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
    fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;

    if (-1 == xioctl(fd, VIDIOC_S_FMT, &fmt)){
            //return err("VIDIOC_S_FMT");
            if (-1 == xioctl(fd, VIDIOC_G_FMT, &fmt)) err("VIDIOC_S_FMT && VIDIOC_G_FMT");
    }

    if(fmt.fmt.pix.pixelformat == V4L2_PIX_FMT_YUYV){
        pixelformat = V4L2_PIX_FMT_YUYV;
        qDebug() << "V4L2_PIX_FMT_YUYV";
    }else if(fmt.fmt.pix.pixelformat == V4L2_PIX_FMT_UYVY){
        pixelformat = V4L2_PIX_FMT_UYVY;
        qDebug() << "V4L2_PIX_FMT_UYVY";
    }else {
        qWarning()  << "Unsupproted video pixelformat format: "
                    <<  ((char)(fmt.fmt.pix.pixelformat >> 0))
                    <<  ((char)(fmt.fmt.pix.pixelformat >> 8))
                    <<  ((char)(fmt.fmt.pix.pixelformat >> 16))
                    <<  ((char)(fmt.fmt.pix.pixelformat >> 24));
        return err("Unsupproted pixelformat format");
    }

    /* Note VIDIOC_S_FMT may change width and height. */

    /* Buggy driver paranoia. */
    min = fmt.fmt.pix.width * 2;
    if (fmt.fmt.pix.bytesperline < min)
        fmt.fmt.pix.bytesperline = min;
    min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
    if (fmt.fmt.pix.sizeimage < min)
        fmt.fmt.pix.sizeimage = min;

    height = fmt.fmt.pix.height;
    width = fmt.fmt.pix.width;


    switch (io) {
    case IO_METHOD_READ:
        if(!initRead(fmt.fmt.pix.sizeimage)) return false;
        break;

    case IO_METHOD_MMAP:
        if(!initMmap()) return false;
        break;

    case IO_METHOD_USERPTR:
        if(!initUserp(fmt.fmt.pix.sizeimage)) return false;
        break;
    }

    // START CAPTURE
    unsigned int i;
    enum v4l2_buf_type type;

    switch (io) {
    case IO_METHOD_READ:
        /* Nothing to do. */
        break;

    case IO_METHOD_MMAP:
        for (i = 0; i < n_buffers; ++i) {
            struct v4l2_buffer buf;

            CLEAR(buf);
            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index = i;

            if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                return err("VIDIOC_QBUF");
        }
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
            return err("VIDIOC_STREAMON");
        break;

    case IO_METHOD_USERPTR:
        for (i = 0; i < n_buffers; ++i) {
            struct v4l2_buffer buf;

            CLEAR(buf);
            buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_USERPTR;
            buf.index = i;
            buf.m.userptr = (unsigned long)buffers[i].start;
            buf.length = buffers[i].length;

            if (-1 == xioctl(fd, VIDIOC_QBUF, &buf))
                return err("VIDIOC_QBUF");

        }
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (-1 == xioctl(fd, VIDIOC_STREAMON, &type))
            return err("VIDIOC_STREAMON");
        break;
    }
    ok = true;
    return true;
}


void V4L2Cap::getFmt(){
    enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    struct v4l2_fmtdesc fmt;
    struct v4l2_frmsizeenum frmsize;

    CLEAR(fmt);

    fmt.index = 0;
    fmt.type = type;

    width = 320;
    height = 240;
    unsigned int maxArea = width * height;

    while (ioctl(fd, VIDIOC_ENUM_FMT, &fmt) >= 0) {
        frmsize.pixel_format = fmt.pixelformat;
        frmsize.index = 0;
        while (ioctl(fd, VIDIOC_ENUM_FRAMESIZES, &frmsize) >= 0) {
            if (frmsize.type == V4L2_FRMSIZE_TYPE_DISCRETE) {
                if(frmsize.discrete.width * frmsize.discrete.height > maxArea){
                    width = frmsize.discrete.width;
                    height = frmsize.discrete.height;
                    maxArea = width * height;
                }
                //fprintf(stderr,"%dx%d\n",frmsize.discrete.width,frmsize.discrete.height);
            } else if (frmsize.type == V4L2_FRMSIZE_TYPE_STEPWISE) {
                if(frmsize.stepwise.max_width * frmsize.stepwise.max_height > maxArea){
                    width = frmsize.stepwise.max_width;
                    height = frmsize.stepwise.max_height;
                    maxArea = width * height;
                }
                //fprintf(stderr,"%dx%d\n",frmsize.stepwise.max_width, frmsize.stepwise.max_height);
            }
            frmsize.index++;
        }
        fmt.index++;
    }
   // fprintf(stderr,"END FMT -----(%dx%d)----",width,height);
}

/// ERRR

bool V4L2Cap::err(const char *msg)
{
    ok = false;
    //error = msg;
    qDebug() << msg;
    //fprintf(stderr,"%s\n", msg);

    if(autoReload){
        reload();
    }
    return ok;
}
