#include "smartconv.h"

SmartConv::SmartConv()
{
}

QByteArray SmartConv::MatToJpg(cv::Mat src, int quality)
{
    QByteArray * resultP = MatToJpgP(src, quality);
    QByteArray result(*resultP);
    delete resultP;
    return result;
}


QByteArray * SmartConv::MatToJpgP(cv::Mat src, int quality)
{
    if(src.empty()) return new QByteArray();
    unsigned char *outbuffer = 0;
    unsigned long outlen = 0;

    unsigned char *outdata = (uchar *) src.data;

    struct jpeg_compress_struct cinfo;// = {0};

    struct jpeg_error_mgr jerr;
    JSAMPROW row_ptr[1];
    int row_stride;

    outbuffer = NULL;
    outlen = 0;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);
    jpeg_mem_dest(&cinfo, &outbuffer, &outlen);

    cinfo.image_width = src.cols;
    cinfo.image_height = src.rows;
    cinfo.input_components = src.channels();

    if(src.channels() == 1){
        cinfo.in_color_space = JCS_GRAYSCALE;
    }else{
        cv::cvtColor( src, src, cv::COLOR_BGR2RGB);
        cinfo.in_color_space = JCS_RGB;
    }

    jpeg_set_defaults(&cinfo);
    jpeg_set_quality(&cinfo, quality, TRUE);

    jpeg_start_compress(&cinfo, TRUE);
    row_stride = src.cols * src.channels();

    while (cinfo.next_scanline < cinfo.image_height)
    {
        row_ptr[0] = &outdata[cinfo.next_scanline * row_stride];
        jpeg_write_scanlines(&cinfo, row_ptr, 1);
    }

    jpeg_finish_compress(&cinfo);
    jpeg_destroy_compress(&cinfo);

    QByteArray * result = new QByteArray((char*)outbuffer,outlen);
    free(outbuffer);

    return result;
}

cv::Mat * SmartConv::JpgToMatP(QByteArray * src)
{
    if(src == 0 || src->isEmpty()){
        return new cv::Mat();
    }
    int err;
    // Variables for the source jpg
    unsigned long jpg_size = src->size();
    unsigned char * jpg_buffer = (unsigned char*)src->data();

    // Variables for the decompressor itself
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;

    int row_stride, width, height, pixel_size;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);
    jpeg_mem_src(&cinfo, jpg_buffer, jpg_size);

    err = jpeg_read_header(&cinfo, TRUE);
    if (err != 1) {
        qWarning("Failed to decompres smartframe jpeg");
        //jpeg_finish_decompress(&cinfo);
        jpeg_destroy_decompress(&cinfo);
        return new cv::Mat();
    }

    jpeg_start_decompress(&cinfo);

    width = cinfo.output_width;
    height = cinfo.output_height;
    pixel_size = cinfo.output_components;
    if(pixel_size != 1){
        qWarning("Wrong number of colors in smartframe");
        //jpeg_finish_decompress(&cinfo);
        jpeg_destroy_decompress(&cinfo);
        return new cv::Mat();
    }
    cv::Mat * result = new cv::Mat( cv::Size(width, height), CV_8UC1);
    row_stride = width * pixel_size;

    while (cinfo.output_scanline < cinfo.output_height) {
        unsigned char *buffer_array[1];
        buffer_array[0] = result->data + (cinfo.output_scanline) * row_stride;
        jpeg_read_scanlines(&cinfo, buffer_array, 1);
    }

    jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    return result;
}
