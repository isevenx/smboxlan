INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += $$PWD/smartframe.h \
    obj/smartframestat.h \
    obj/smartframepack.h \
    obj/smartlog.h \
    obj/smarthistory.h \
    obj/smartfiledownloader.h

SOURCES += $$PWD/smartframe.cpp \
    obj/smartframestat.cpp \
    obj/smartframepack.cpp \
    obj/smartlog.cpp \
    obj/smarthistory.cpp \
    obj/smartfiledownloader.cpp
