#ifndef SMARTFRAMEPACK_H
#define SMARTFRAMEPACK_H

#include <QObject>
#include <QNetworkReply>
#include <QHttpMultiPart>
#include <QMetaMethod>
#include <QTimer>
#include <QSettings>
#include <QCryptographicHash>

#include <vector>

#include "smartframe.h"
#include "settings/smartsettings.h"


class SmartFramePack : public QObject
{
    Q_OBJECT
public:
    explicit SmartFramePack(QNetworkAccessManager * manager, std::vector<SmartFrame *> frames, SmartSettings settings = SmartSettings(), QObject *parent = 0);
    ~SmartFramePack();
    void send();
    std::vector<SmartFrame *> removeFrames();

signals:
    void finished(SmartFramePack *);
    void failed(SmartFramePack *);

public slots:

private slots:
    void nrError(QNetworkReply::NetworkError code);
    void nrFinished();
    void nrSslErrors(const QList<QSslError> & errors);
    void makeRequest();

private:
    std::vector<SmartFrame *> frames;

    QSettings globSettings;
    QNetworkAccessManager * manager;
    QNetworkReply * reply;
    SmartSettings settings;
    QHttpMultiPart * multiPart;

    void done();
    void abort();
    QHttpMultiPart * getMultipart();
    int attempts;
    bool isSending;
};

#endif // SMARTFRAMEPACK_H
