#include "smartlog.h"

SmartLog::SmartLog(){
//    logFile = QFile("log.log");
    logPath = "log/";
    logDir = QDir(logPath);
    started = QDateTime::currentDateTime().toMSecsSinceEpoch();
}

SmartLog* SmartLog::smartLog = 0;
SmartLog* SmartLog::instance()
{
    static QMutex mutex;
    if (!smartLog)
    {
        mutex.lock();

        if (!smartLog)
            smartLog = new SmartLog;

        mutex.unlock();
    }

    return smartLog;
}

SmartLog * SmartLog::operator -> (){
    static QMutex mutex;
    if (!smartLog)
    {
        mutex.lock();

        if (!smartLog)
            smartLog = new SmartLog;

        mutex.unlock();
    }

    return smartLog;
}

void SmartLog::drop()
{
    static QMutex mutex;
    mutex.lock();
    delete smartLog;
    smartLog = 0;
    mutex.unlock();
}

void SmartLog::log(QString msg)
{
    log(msg,"info");
}

void SmartLog::log(QString msg, QString type)
{
    log(msg,type, QDateTime::currentDateTime().toMSecsSinceEpoch());
}

void SmartLog::log(QString msg, QString type, qint64 timestamp)
{
    SmartLog * log = SmartLog::instance();
    QString line = QString("%1 | %2 | %3 ").arg(timestamp).arg(type).arg(msg);
    log->writeLog(line);
    log->addLastLogs(line);
}

void SmartLog::addLastLogs(QString line)
{
    QMutexLocker locker(&mutex);
    lastLogs.push_front(line);
    if(lastLogs.size() > 300) lastLogs.pop_back();
}

void SmartLog::writeLog(QString line)
{
    QMutexLocker locker(&mutex);
    if(!logDir.exists()) {
        QDir().mkdir(logDir.path());
    }
    QFile file(logDir.filePath("log.log"));
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
        fprintf(stderr,"Log file not writable");
        return;
    }

    file.write(line.toUtf8());
    file.putChar('\n');
    file.close();
}

QJsonValue SmartLog::getLastLogs()
{
    QMutexLocker locker(&mutex);
    QJsonArray result;
    for( std::deque<QString>::iterator i = lastLogs.begin(); i != lastLogs.end(); ++i)
    {
        //fprintf(stderr,"%s",QString("LastLogs: %1\n").arg((*i) ).toStdString().c_str());
        result.append(*i);
    }
    return result;
}
