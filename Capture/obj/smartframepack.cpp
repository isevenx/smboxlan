#include "smartframepack.h"

SmartFramePack::SmartFramePack(QNetworkAccessManager * manager, std::vector<SmartFrame *> frames, SmartSettings settings, QObject *parent) :
    QObject(parent)
{
    reply = 0;
    multiPart = 0;
    this->manager = manager;
    this->settings = settings;
    this->frames = frames;
    attempts = 0;
    isSending = false;
}

SmartFramePack::~SmartFramePack()
{
    qDebug() << "Deleteing smartFrame data";
    for(std::vector<SmartFrame *>::iterator f = frames.begin(); f != frames.end(); ++f){
        delete (*f);
    }
}

std::vector<SmartFrame *> SmartFramePack::removeFrames()
{
    std::vector<SmartFrame *> clone(frames.begin(),frames.end());
    frames.clear();
    return clone;
}

void SmartFramePack::send()
{
    if(isSending) return;
    isSending = true;

    makeRequest();
}

void SmartFramePack::makeRequest()
{
    QUrl url(settings.serverUrl);
    url.setPath(settings.sender.frameSendUrl);

    QNetworkRequest request(url);
    QHttpMultiPart * mp  = getMultipart();
    reply = manager->post(request, mp);
    reply->setParent(this);

    connect(reply, SIGNAL(finished()), this, SLOT(nrFinished()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(nrError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(nrSslErrors(QList<QSslError>)));
}

QHttpMultiPart * SmartFramePack::getMultipart()
{
    if(multiPart != 0){
        delete multiPart;
    }

    QHttpPart textPart;
    QHttpPart imagePart;

    multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    multiPart->setParent(this);

    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"hash\""));
    textPart.setBody("hashtext");
    multiPart->append(textPart);

    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"deviceId\""));
    textPart.setBody(globSettings.value("SMB/id","125").toString().toUtf8());
    multiPart->append(textPart);

    int i = -1;
    for(std::vector<SmartFrame *>::iterator f = frames.begin(); f != frames.end(); ++f){
        i++;
        textPart.setHeader(QNetworkRequest::ContentDispositionHeader,
                           QVariant(QString("form-data; name=\"frames[%1].tv.timestamp\"").arg(i)));
        textPart.setBody(QString::number((*f)->stat.tvCaptured).toStdString().c_str());
        multiPart->append(textPart);


        /// tv.full image data JPG
        if(settings.processor.keepFullTv && (*f)->getTv() != 0){
            imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
            imagePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                QVariant(QString("form-data; name=\"frames[%1].tv.full\"; filename=\"image.jpg\"").arg(i)));
            imagePart.setBody(*((*f)->getTv()));
            multiPart->append(imagePart);
        }

        textPart.setHeader(QNetworkRequest::ContentDispositionHeader,
                           QVariant(QString("form-data; name=\"frames[%1].wc.timestamp\"").arg(i)));
        textPart.setBody(QString::number((*f)->stat.wcCaptured).toStdString().c_str());
        multiPart->append(textPart);

        /// tv.part image data JPG
        if((*f)->getTvPart() != 0){
            imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
            imagePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                QVariant(QString("form-data; name=\"frames[%1].tv.channel\"; filename=\"image.jpg\"").arg(i)));
            imagePart.setBody(*((*f)->getTvPart()));
            multiPart->append(imagePart);
        }
        /// tv.partunif image data JPG
        if((*f)->getTvPartUnif() != 0){
            imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
            imagePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                QVariant(QString("form-data; name=\"frames[%1].tv.unif\"; filename=\"image.jpg\"").arg(i)));
            imagePart.setBody(*((*f)->getTvPartUnif()));
            multiPart->append(imagePart);
        }


        /// wc.full image data JPG
        if(settings.processor.keepFullWc && (*f)->getWc() != 0){
            imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
            imagePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                QVariant(QString("form-data; name=\"frames[%1].wc.full\"; filename=\"image.jpg\"").arg(i)));
            imagePart.setBody(*((*f)->getWc()));
            multiPart->append(imagePart);
        }

        std::vector<QByteArray*> faces = (*f)->getFaces();
        int fi = 0;
        QString hash;
        for(std::vector<QByteArray*>::iterator f = faces.begin(); f != faces.end(); ++f)
        {
            /// tv.full image data JPG
            imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
            imagePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                QVariant(QString("form-data; name=\"frames[%1].wc.faces[%2]\"; filename=\"image.jpg\"").arg(i).arg(fi)));
            imagePart.setBody(*(*f));
            multiPart->append(imagePart);

            hash = QString(QCryptographicHash::hash(*(*f),QCryptographicHash::Md5).toHex());
            textPart.setHeader(QNetworkRequest::ContentDispositionHeader,
                               QVariant(QString("form-data; name=\"frames[%1].wc.faceHash[%2]\"").arg(i).arg(fi)));
            textPart.setBody(hash.toStdString().c_str());
            multiPart->append(textPart);
            fi++;
        }
    }
    return multiPart;
}

void SmartFramePack::nrError(QNetworkReply::NetworkError code __attribute__ ((unused)))
{
    qDebug() << "Network error: " << reply->errorString();
}

void SmartFramePack::nrSslErrors(const QList<QSslError> & errors __attribute__ ((unused)))
{
    qDebug() << "SSL ERROR";
    reply->ignoreSslErrors();
}

void SmartFramePack::nrFinished()
{
    if(reply->error() != QNetworkReply::NoError){
        if(++attempts > 1){
            qDebug() << "Network request FAIL, maxAttempts exeeded DROPING DATA";
            abort();
        }else{
            qDebug() << "Network request FAIL retrying";
            QTimer::singleShot(5000,this,SLOT(makeRequest()));
        }
    }else{
        qDebug() << "Network request successful";
        done();
    }
}

void SmartFramePack::abort()
{
    static const QMetaMethod failedSignal = QMetaMethod::fromSignal(&SmartFramePack::failed);
    if (this->isSignalConnected(failedSignal)) {
        emit failed(this);
    }else{
        deleteLater();
    }
}


void SmartFramePack::done()
{
    static const QMetaMethod finishedSignal = QMetaMethod::fromSignal(&SmartFramePack::finished);
    if (this->isSignalConnected(finishedSignal)) {
        emit finished(this);
    }else{
        deleteLater();
    }
}
