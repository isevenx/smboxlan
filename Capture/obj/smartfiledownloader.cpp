#include "smartfiledownloader.h"

SmartFileDownloader::SmartFileDownloader(QString url, QObject *parent, QNetworkAccessManager *manager):
    QObject(parent)
{
    constructor(url,QMap<QString,SmartPostValue>(),QMap<QString,QString>(),manager);
}

SmartFileDownloader::SmartFileDownloader(QString url, QMap<QString, SmartPostValue> values, QObject *parent, QNetworkAccessManager *manager):
    QObject(parent)
{
    constructor(url,values,QMap<QString,QString>(),manager);
}

SmartFileDownloader::SmartFileDownloader(QString url, QMap<QString, SmartPostValue> values, QMap<QString, QString> headers, QObject *parent, QNetworkAccessManager *manager):
    QObject(parent)
{
    constructor(url,values,headers,manager);
}

void SmartFileDownloader::constructor(QString url, QMap<QString, SmartPostValue> values, QMap<QString, QString> headers, QNetworkAccessManager *manager)
{
    if(manager == 0) manager = new QNetworkAccessManager(this);
    isSending = false;
    aborted = false;
    reply = 0;
    attempts = 0;
    maxAttemts = 3;
    this->url = url;
    this->values = values;
    this->headers = headers;
    this->manager = manager;
}

SmartFileDownloader::~SmartFileDownloader()
{
    qDebug() << "Deleteing SmartFacesPack data";
}

void SmartFileDownloader::send()
{
    if(isSending) return;
    isSending = true;

    makeRequest();
}

void SmartFileDownloader::makeRequest()
{
    QNetworkRequest request(url);
    for(QMap<QString, QString>::iterator head = headers.begin(); head != headers.end(); ++head)
    {
        request.setRawHeader(head.key().toUtf8(),head.value().toUtf8());
    }

    QHttpMultiPart * mp  = getMultipart();
    if(mp != 0){
        reply = manager->post(request, mp);
        mp->setParent(reply);
        reply->setParent(this);
    }else{
        reply = manager->get(request);
        reply->setParent(this);
    }

    connect(reply, SIGNAL(finished()), this, SLOT(nrFinished()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(nrError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(nrSslErrors(QList<QSslError>)));
}

QHttpMultiPart * SmartFileDownloader::getMultipart()
{
    if(values.isEmpty()) return 0;

    QHttpPart textPart;
    QHttpPart imagePart;

    QHttpMultiPart * multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    multiPart->setParent(this);

    for(QMap<QString, SmartPostValue>::iterator val = values.begin(); val != values.end(); ++val)
    {
        if(val.value().isString()){
            textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\""+val.key()+"\""));
            textPart.setBody(val.value().toString().toUtf8());
            multiPart->append(textPart);
        }else{
            imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
            imagePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                QVariant(QString("form-data; name=\""+val.key()+"\"; filename=\"somefile.jpg\"")));
            imagePart.setBody(val.value().toByteArray());
            multiPart->append(imagePart);
        }
    }
    return multiPart;
}

void SmartFileDownloader::nrError(QNetworkReply::NetworkError code __attribute__ ((unused)))
{
    qDebug() << "Network error: " << reply->errorString();
}

void SmartFileDownloader::nrSslErrors(const QList<QSslError> & errors __attribute__ ((unused)))
{
    qDebug() << "SSL ERROR";
    reply->ignoreSslErrors();
}

void SmartFileDownloader::nrFinished()
{
    if(!aborted)
    {
        if(reply->error() != QNetworkReply::NoError){
            if(++attempts > maxAttemts){
                qDebug() << "Network request FAIL, maxAttempts exeeded DROPING DATA";
                abort();
            }else{
                qDebug() << "Network request FAIL retrying";
                QTimer::singleShot(3000,this,SLOT(makeRequest()));
            }
        }else{
            result = reply->readAll();
            qDebug() << "Network request successful";
            done();
        }
    }
}

void SmartFileDownloader::abort()
{
    static const QMetaMethod failedSignal = QMetaMethod::fromSignal(&SmartFileDownloader::failed);
    if (this->isSignalConnected(failedSignal)) {
        emit failed(this);
    }else{
        deleteLater();
    }
}

void SmartFileDownloader::done()
{
    static const QMetaMethod finishedSignal = QMetaMethod::fromSignal(&SmartFileDownloader::finished);
    if (this->isSignalConnected(finishedSignal)) {
        emit finished(this);
    }else{
        deleteLater();
    }
}

void SmartFileDownloader::stop()
{
    aborted = true;
    if(reply != 0 && !reply->isFinished()) reply->abort();
}
