#include "smartframestat.h"

SmartFrameStat::SmartFrameStat(){
    wcCaptured = tvCaptured = procStart = procEnd = frameSent = QDateTime::currentDateTime().toMSecsSinceEpoch();
    send_attempts = 0;
    threadCount = 1;
    isProcessed = isSent = hasTv = hasWc = isSaved = false;
    fpm = 0;
}
