#ifndef SMARTFRAME_H
#define SMARTFRAME_H

#include <vector>

#include <QDateTime>
#include <QByteArray>
#include <QDir>
#include <QMutex>
#include <QMutexLocker>

#include "opencv2/imgproc/imgproc.hpp"
#include "picparse.h"

#include "smartframestat.h"

class SmartFrame
{
public:
    SmartFrame();
    SmartFrame(const SmartFrame& other);
    ~SmartFrame();

    static bool storeCompressed;
    static bool sendFullTv;
    static bool sendFullWc;
    static QString tmpDir;

    SmartFrameStat stat;

    void release();
    void free();

    bool setTv(cv::Mat * tv);
    bool setWc(cv::Mat * wc);

    void process(PicParse * parser);

    QByteArray * getTv();
    QByteArray * getWc();

    QByteArray * getTvPart();
    QByteArray * getTvPartUnif();

    cv::Mat * getTvMat();
    cv::Mat * getWcMat();

    void save();
    inline int facesSize(){ QMutexLocker locker(&mutex); return faces.size();}

    std::vector<QByteArray *> getFaces();
    static std::vector<SmartFrame *> load();

private:

    void setFaces(std::vector<QByteArray *> faces);
    std::vector<QByteArray * > faces;

    cv::Mat * tvMat;
    cv::Mat * wcMat;

    QByteArray * tv;
    QByteArray * wc;
    QByteArray * tvPart;
    QByteArray * tvPartUnif;

    mutable QMutex mutex;

};

#endif // SMARTFRAME_H
