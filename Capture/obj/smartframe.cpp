#include "smartframe.h"

bool SmartFrame::storeCompressed = true;
bool SmartFrame::sendFullTv = true;
bool SmartFrame::sendFullWc = true;
QString SmartFrame::tmpDir = "SmartFrames";

SmartFrame::SmartFrame()
{
    tv = wc = 0;
    tvMat = wcMat = 0;
    tvPart = tvPartUnif = 0;
}

SmartFrame::SmartFrame(const SmartFrame& other)
{
    QMutexLocker locker(&mutex);
    this->faces = other.faces;
    this->stat = other.stat;
    this->tv = other.tv;
    this->wc = other.wc;
    this->tvMat = other.tvMat;
    this->wcMat = other.wcMat;
    this->tvPart = other.tvPart;
}

SmartFrame::~SmartFrame()
{
    release();
}

void SmartFrame::release()
{
    QMutexLocker locker(&mutex);
    if(tv != 0) delete tv;
    if(wc != 0) delete wc;
    if(tvMat != 0) delete tvMat;
    if(wcMat != 0) delete wcMat;
    if(tvPart != 0) delete tvPart;
    if(tvPartUnif != 0) delete tvPartUnif;

    for(unsigned int i = 0; i < faces.size(); i ++){
        delete faces[i];
    }
    std::vector<QByteArray *>().swap(faces);
    tv = wc = 0;
    tvMat = wcMat = 0;
    tvPart = tvPartUnif = 0;
}

void SmartFrame::free()
{
    QMutexLocker locker(&mutex);
    if(storeCompressed){
        if(tv == 0 && tvMat != 0) tv = SmartConv::MatToJpgP(*tvMat);
        if(wc == 0 && wcMat != 0) wc = SmartConv::MatToJpgP(*wcMat);
        if(tvMat != 0) delete tvMat;
        if(wcMat != 0) delete wcMat;
        tvMat = 0;
        wcMat = 0;
    }
}


bool SmartFrame::setTv(cv::Mat * mat)
{
    QMutexLocker locker(&mutex);
    if(mat == 0 || mat->empty()) return false;
    if(tv != 0) delete tv;
    if(tvMat != 0) delete tvMat;
    tv = 0;
    tvMat = 0;
    if(storeCompressed){
        tv = SmartConv::MatToJpgP(*mat);
        delete mat;
    }else{
        tvMat = mat;
    }
    stat.hasTv = true;
    return true;
}

bool SmartFrame::setWc(cv::Mat * mat)
{
    QMutexLocker locker(&mutex);
    if(mat == 0 || mat->empty()) return false;
    if(wc != 0) delete wc;
    if(wcMat != 0) delete wcMat;
    wc = 0;
    wcMat = 0;
    if(storeCompressed){
        wc = SmartConv::MatToJpgP(*mat);
        delete mat;
    }else{
        wcMat = mat;
    }
    stat.hasWc = true;
    return true;
}

cv::Mat * SmartFrame::getTvMat()
{
    QMutexLocker locker(&mutex);
    if(tvMat != 0){
        return tvMat;
    }else if(tv != 0){
        tvMat = SmartConv::JpgToMatP(tv);
        return tvMat;
    }
    tvMat = new cv::Mat();
    return tvMat;
}

cv::Mat * SmartFrame::getWcMat()
{
    QMutexLocker locker(&mutex);
    if(wcMat != 0){
        return wcMat;
    }else if(wc != 0){
        wcMat = SmartConv::JpgToMatP(wc);
        return wcMat;
    }
    wcMat = new cv::Mat();
    return wcMat;
}

QByteArray * SmartFrame::getTv()
{
    QMutexLocker locker(&mutex);
    if(tv != 0){
        return tv;
    }else if(tvMat != 0){
        tv = SmartConv::MatToJpgP(*tvMat);
        return tv;
    }
    tv = new QByteArray();
    return tv;
}


QByteArray *SmartFrame::getWc()
{
    QMutexLocker locker(&mutex);
    if(wc != 0){
        return wc;
    }else if(wcMat != 0){
        wc = SmartConv::MatToJpgP(*tvMat);
        return wc;
    }
    wc = new QByteArray();
    return wc;
}

QByteArray * SmartFrame::getTvPart()
{
    return tvPart;
}

QByteArray * SmartFrame::getTvPartUnif()
{
    return tvPartUnif;
}

void SmartFrame::setFaces(std::vector<QByteArray*> faces)
{
    QMutexLocker locker(&mutex);
    this->faces = faces;
}

void SmartFrame::save()
{
    QDir dir(QString("%1/%2/").arg(tmpDir).arg(stat.tvCaptured).toStdString().c_str());
    if(!dir.exists()){
        if(!dir.mkpath(".")){
            qCritical() << "Could not create directory for dumping faces: " << dir.path().toUtf8().constData();
            return;
        }
    }

    for(unsigned int i = 0; i < faces.size(); i++)
    {
        try
        {
            QFile file(dir.filePath(QString("face%1.jpg").arg(i)).toStdString().c_str());
            file.open(QIODevice::WriteOnly);
            file.write(*(faces[i]));
            file.close();
        }
        catch (std::runtime_error& ex)
        {
            qCritical() << "Could not save face img: " << ex.what();
            return;
        }
    }

    try
    {
        QFile file(dir.filePath(QString("tv_%1.jpg").arg(stat.tvCaptured).toStdString().c_str()));
        file.open(QIODevice::WriteOnly);
        file.write(*getTv());
        file.close();
    }
    catch (std::runtime_error& ex)
    {
        qCritical() << "Could not save tv frame: " <<  ex.what();
        return;
    }

    try
    {
        QFile file(dir.filePath(QString("wc_%1.jpg").arg(stat.wcCaptured).toStdString().c_str()));
        file.open(QIODevice::WriteOnly);
        file.write(*getWc());
        file.close();
    }
    catch (std::runtime_error& ex)
    {
        qCritical() << "Could not save tv frame: " << ex.what();
        return;
    }


    if(getTvPart() != 0){
        try
        {
            QFile file(dir.filePath(QString("tv_part_%1.jpg").arg(stat.tvCaptured).toStdString().c_str()));
            file.open(QIODevice::WriteOnly);
            file.write(*getTvPart());
            file.close();
        }
        catch (std::runtime_error& ex)
        {
            qCritical() << "Could not save tv part: " << ex.what();
            return;
        }
    }
}

std::vector<SmartFrame *> SmartFrame::load()
{
    return std::vector<SmartFrame *>();
}

std::vector<QByteArray * > SmartFrame::getFaces()
{
    QMutexLocker locker(&mutex);
    return faces;
}

void SmartFrame::process(PicParse * parser)
{
    QMutexLocker locker(&mutex);
    stat.procStart = QDateTime::currentDateTime().toMSecsSinceEpoch();
    locker.unlock();
    parser->ParseFaces(*(getWcMat()));
    if(tvPart != 0) delete tvPart;
    tvPart = parser->getTvPart(*(getTvMat()));
    tvPartUnif = parser->getTvPartUnif(*(getTvMat()));
    tv = parser->getScaledTv(*(getTvMat()),tv);
    wc = parser->getScaledWc(*(getWcMat()),wc);
    free();
    locker.relock();
    this->faces = parser->GetFacesJpg();
    stat.isProcessed = true;
    stat.procEnd = QDateTime::currentDateTime().toMSecsSinceEpoch();
}

