#include <QtDebug>
#include <QSettings>
#include <QFile>
#include <QTextStream>
#include <stdio.h>
#include <QSharedMemory>
#include <stdlib.h>

#include "obj/smartlog.h"
#include "dispatcher.h"

void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    QString txt;
    switch (type) {
    case QtDebugMsg:
        SmartLog::log(msg,"Debug");
        txt = QString("Debug: %1").arg(localMsg.constData());
        fprintf(stderr,"%s",QString("%1\n").arg(txt).toStdString().c_str());
        break;
    case QtWarningMsg:
        SmartLog::log(msg,"Warning");
        txt = QString("Warning: %1").arg(localMsg.constData());
        fprintf(stderr,"%s",QString("%1\n").arg(txt).toStdString().c_str());
        break;
    case QtCriticalMsg:
        SmartLog::log(msg,"Critical");
        txt = QString("Critical: %1 (%2:%3, %4)").arg(localMsg.constData()).arg(context.file).arg(context.line).arg(context.function);
        fprintf(stderr,"%s",QString("%1\n").arg(txt).toStdString().c_str());
        break;
    case QtFatalMsg:
        SmartLog::log(msg,"Fatal");
        txt = QString("Fatal: %1 (%2:%3, %4)").arg(localMsg.constData()).arg(context.file).arg(context.line).arg(context.function);
        fprintf(stderr,"%s",QString("%1\n").arg(txt).toStdString().c_str());
        abort();
    }

    QFile outFile("debuglog.txt");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << txt << endl;
}

int main(int argc, char *argv[])
{
    //QFile outFile("debuglog.txt");
    //if(outFile.exists()) outFile.remove();
    bool isRunning = false;
    QSharedMemory sharedMemory;
    sharedMemory.setKey("1VS91sMl6N9Jud7M3zBf");
    if(sharedMemory.attach(QSharedMemory::ReadOnly)) {
        sharedMemory.detach();
        isRunning = false;
    }

    if (!sharedMemory.create(1)) {
        isRunning = true;
    }
    if(isRunning){
        fprintf(stderr,"============= Allready running =============\n");
        return 0;
    }

    qInstallMessageHandler(customMessageHandler);


    QCoreApplication::setOrganizationName("Applyit");
    QCoreApplication::setOrganizationDomain("applyit.lv");
    QCoreApplication::setApplicationName("SMBBox");

    Dispatcher a(argc, argv);
    Dispatcher::setup_unix_signal_handlers();

    a.startCapturing();

    return a.exec();
}
