#include "framesender.h"

FrameSender::FrameSender(QObject *parent) :
    SmartService(parent)
{
    manager = new QNetworkAccessManager(this);
    terminateRequested = false;
    requestCount = 0;
}

FrameSender::~FrameSender()
{
}

void FrameSender::doTerminate(bool force)
{
    qDebug("Finishing FrameSender!");
    if(force || (requestCount == 0 && frames.size() == 0)){
        for(unsigned int i = 0; i < frames.size(); i ++){
            delete frames[i];
        }
        emit finished();
        return;
    }

    terminateRequested = true;
    if(frames.size() > 0){
        SendFrames();
    }else{
        emit finished();
    }
}

void FrameSender::load(SmartSettings settings)
{
    bool settingsChanged = settings.sender != this->settings.sender;
    this->settings = settings;
    if(settingsChanged) stopAndRelaod();
}

void FrameSender::stopAndRelaod()
{

}

void FrameSender::SendFrame(SmartFrame *frame)
{
    //frame->save();
    frames.push_back(frame);
    clearQueue();
    SendFrames();
}

void FrameSender::SendFrames()
{
    if(!terminateRequested && frames.size() < (unsigned int) settings.sender.minFrames)  return;
    if(frames.empty()) return;
    if(requestCount > 6){
        if(requestCount % 5 == 0) qDebug() << "Too many paralel requests, waiting, Queue size: "<< frames.size();
        return;
    }
    requestCount++;

    SmartFrame * frame;
    std::vector< SmartFrame *> packFrames;
    int count = 0;
    while(!frames.empty() && count++ < settings.sender.maxFrames){
        frame = *(frames.begin());
        frames.pop_front();
        packFrames.push_back(frame);
    }
    SmartFramePack * package = new SmartFramePack(manager,packFrames,settings,this);

    connect(package, SIGNAL(finished(SmartFramePack *)), this, SLOT(packageSent(SmartFramePack *)));
    connect(package, SIGNAL(failed(SmartFramePack *)), this, SLOT(packageError(SmartFramePack *)));
    package->send();
}


void FrameSender::packageSent(SmartFramePack * package)
{

    requestCount--;
    package->deleteLater();
    qDebug() << "Queue size: "<< frames.size() << (terminateRequested?" terminateRequested ":"");
    SendFrames();
    if(terminateRequested && requestCount == 0 && frames.size() == 0){
         qDebug() << "Sender finsihed ";
         emit finished();
    }else if(terminateRequested){
        qDebug() << "Sender  cant finsih reqCount: "<< requestCount << " queue: " << frames.size();
    }
}

void FrameSender::packageError(SmartFramePack * package)
{
    requestCount--;
    package->deleteLater();
    std::vector< SmartFrame *> packFrames = package->removeFrames();
    frames.insert(frames.begin(),packFrames.begin(),packFrames.end());
    clearQueue();

    qDebug() << "Adding frames back to SEND queue, queue size: "<< frames.size();
    SendFrames();
}

void FrameSender::clearQueue()
{
    SmartFrame * f;
    while(frames.size() > (unsigned int) settings.sender.maxQueue){
        f = *(frames.begin());
        delete f;
        frames.pop_front();
    }
}
