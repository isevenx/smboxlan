#ifndef PICPARSER_H
#define PICPARSER_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QMutex>

#include "picparse.h"
#include "obj/smartframe.h"
#include "obj/smarthistory.h"
#include "settings/smartsettings.h"

class PicParser : public QObject
{
    Q_OBJECT
public:
    PicParser(SmartSettings settings, int id = 0, QObject *parent = 0);
    ~PicParser();

signals:
    void PicParsed(SmartFrame * frame, int id);

public slots:
    void ParsePic(SmartFrame * frame);

private:
    PicParse  * parser;
    int id;
    SmartSettings settings;

    mutable QMutex mutex;
};

#endif // PICPARSER_H
