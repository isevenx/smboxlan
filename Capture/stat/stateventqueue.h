#ifndef STATEVENTQUEUE_H
#define STATEVENTQUEUE_H

#include <QDebug>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QByteArray>

#include <deque>
#include "stat/statevent.h"

class StatEventQueue
{
public:
    StatEventQueue();
    unsigned int maxEvents;
    quint64 eventsExpireAfter;

    void addEvent(StatEvent event);

    std::deque<StatEvent> queue;

    QJsonValue getJson();

private:
    QJsonValue getJson(std::deque<StatEvent>::iterator begin, std::deque<StatEvent>::iterator end);
};

#endif // STATEVENTQUEUE_H
