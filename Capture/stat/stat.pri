INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

HEADERS += $$PWD/statevent.h \
    $$PWD/stateventqueue.h

SOURCES += $$PWD/statevent.cpp \
    $$PWD/stateventqueue.cpp
