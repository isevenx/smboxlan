#include "statevent.h"

StatEvent::StatEvent()
{
    init();
}

StatEvent::StatEvent(QString type, QString message)
{
    init(type, message);
}


StatEvent::StatEvent(QString type, QString message, quint64 time)
{
    init(type, message, time);
}

void StatEvent::init(QString type, QString message, quint64 time)
{
    this->type = type;
    this->message = message;
    this->time = time;
}

