#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <unistd.h>
#include <csignal>
#include <sys/socket.h>

#include <QCoreApplication>
#include <QSocketNotifier>

#include "framegrabber.h"
#include "frameprocessor.h"
#include "framesender.h"
#include "commandandcontrol.h"
#include "stat/statevent.h"
#include "arduinointerface.h"


/**
 * @brief WHOIS in command? I AM!
 */
class Dispatcher : public QCoreApplication
{
    Q_OBJECT
public:
    explicit Dispatcher(int &argc, char **argv);
    void startCapturing();

    // Unix signal handlers.
    static int setup_unix_signal_handlers();

signals:
    //void doTerminate(bool force);

public slots:
    void threadFinished();
    void reload(SmartSettings settings);
    SmartSettings getSettings();

    // Qt signal handlers.
    void handleSigInt();
    void handleSigTerm();

private:
    FrameGrabber frameGrabber;
    FrameProcessor frameProcessor;
    FrameSender frameSender;
    CommandAndControl controller;
    ArduinoInterface arduino;

    QThread grabberThread;
    QThread processorThread;
    QThread senderThread;
    QThread controlThread;

    static int sigintpFd[2];
    static int sigtermFd[2];

    QSocketNotifier *snInt;
    QSocketNotifier *snTerm;

    bool doClose;
    bool forceClose;
    int closeAttempts;

    SmartSettings settings;

    void close();
    static void intSignalHandler(int unused);
    static void termSignalHandler(int unused);
};



#endif // DISPATCHER_H
