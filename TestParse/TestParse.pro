TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

SOURCES += main.cpp

INCLUDEPATH += ../PicParse
LIBS     += -L../PicParse -lPicParse

INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib
LIBS += -lopencv_calib3d -lopencv_contrib -lopencv_features2d \
        -lopencv_flann -lopencv_imgproc -lopencv_ml \
        -lopencv_objdetect -lopencv_video -lopencv_highgui -lopencv_core
